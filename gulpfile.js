var gulp = require("gulp");
var ts = require("gulp-typescript");
var fs = require('fs');
var tsProject = ts.createProject("tsconfig.json");

var libFiles = [
    './node_modules/react/dist/react.js',
    './node_modules/react-dom/dist/react-dom.js',
    './node_modules/systemjs/dist/system.js'
];

var publicFolder = 'public';
var distFolder = 'dist';

function copy () {
    var filename = null;
    var status = fs.lstatSync('./' + publicFolder + '/vendor');
    if (!status.isDirectory()) {
        fs.mkdirSync('./' + publicFolder + '/vendor');
    }
    for (var i = 0; i < libFiles.length; i++) {
        filename = libFiles[i].split('/').pop();
        fs.createReadStream(libFiles[i]).pipe(fs.createWriteStream('./' + publicFolder + '/vendor/' + filename));
    }
}

gulp.task('typescript', function () {
    var tsResult = gulp.src([
        'app/**/*.tsx',
        'app/**/*.ts'
    ])
        .pipe(tsProject())
        .js.pipe(gulp.dest(distFolder));
});

gulp.task("default", [ 'watch' ]);

gulp.task('watch', ['typescript' ], function() {
    gulp.watch([
        'app/**/*.tsx',
        'app/**/*.ts'
    ], ['typescript']);
});
