import * as React from 'react'
import { render } from 'react-dom'
import { Router, Route, browserHistory, IndexRoute } from "react-router"

import Sidebar from './frontend/sidebar'
import PatientIndexComponent from "./frontend/patient-index-component";
import PatientCreateComponent from "./frontend/patient-create-component";
import PatientShowComponent from "./frontend/patient-show-component";
import CureShowComponent from "./frontend/cure-show-component";

import PatientCreateCure from "./frontend/patient-create-cure";

import DoctorShowComponent from "./frontend/doctor/show-component";
import DoctorCreateComponent from "./frontend/doctor/create-component";
import DoctorSingleComponent from "./frontend/doctor/doctor-component";

import MedicamentEditComponent from "./frontend/medicament/edit-component";

import PositionShowComponent from "./frontend/position/show-component";
import PositionCreateComponent from "./frontend/position/create-component";
import PositionEditComponent from "./frontend/position/edit-component";

import DepartmentShowComponent from "./frontend/department/show-component";
import DepartmentCreateComponent from "./frontend/department/create-component";
import DepartmentEditComponent from "./frontend/department/edit-component";

import MedicamentShowComponent from "./frontend/medicament/show-component";
import MedicamentCreateComponent from "./frontend/medicament/create-component";

class FrontendComponent extends React.Component<{}, {}> {

    public render() {
        return (
            <div className="ms-Grid main-grid">
                <div className="ms-Grid-row">
                    <div className="ms-Grid-col ms-u-md3 sidebar-wrapper">
                        <Sidebar />
                    </div>
                    <div className="ms-Grid-col ms-u-md9 content-wrapper">
                        <Router history={browserHistory}>
                            <Route path="/" component={PatientIndexComponent} />
                            <Route path="/patient" component={PatientIndexComponent} />
                            <Route path="/patient/create" component={PatientCreateComponent} />
                            <Route path="/patient/show/:userId" component={PatientShowComponent} />

                            <Route path="/patient/createCure/:userId" component={PatientCreateCure} />

                            <Route path="/cure/show/:cureId" component={CureShowComponent} />

                            <Route path="/doctor" component={DoctorShowComponent} />
                            <Route path="/doctor/create" component={DoctorCreateComponent} />
                            <Route path="/doctor/:id" component={DoctorSingleComponent} />

                            <Route path="/position" component={PositionShowComponent} />
                            <Route path="/position/create" component={PositionCreateComponent} />
                            <Route path="/position/edit/:id" component={PositionEditComponent} />

                            <Route path="/department" component={DepartmentShowComponent} />
                            <Route path="/department/create" component={DepartmentCreateComponent} />
                            <Route path="/department/:id" component={DepartmentEditComponent} />

                            <Route path="/medicament" component={MedicamentShowComponent} />
                            <Route path="/medicament/create" component={MedicamentCreateComponent} />
                            <Route path="/medicament/:id" component={MedicamentEditComponent} />
                        </Router>
                    </div>
                </div>
            </div>
        );
    }
}

render(<FrontendComponent />, document.querySelector('#app'));
