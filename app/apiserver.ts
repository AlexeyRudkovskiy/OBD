import { Connection } from "./orm/connection"
import * as bodyParser from "body-parser"
import * as express from "express";
import patientsRoute from "./api/patient"
import doctorsRoute from "./api/doctor"
import medicamentsRoute from "./api/medicament"
import curesRoute from "./api/cure"
import CRUD from "./api/crud";
import departmentsRoute from "./api/department"
import positionsRoute from "./api/position"

var dbInstance:Connection = Connection.getInstance();
dbInstance.connect();

var application:any = express();

var serverPort = 5000;

application.use('/api', function (req:any, res:any, next:any) {
    req.db = dbInstance;
    next();
});

application.use(bodyParser.urlencoded({ extended: false }));
application.use(bodyParser.json());
application.use(express.static(__dirname + '\\..\\public'));
application.use('/dist', express.static(__dirname + '\\'));
application.use('/node_modules', express.static(__dirname + '\\..\\node_modules\\'));

application.use('/api/patient', patientsRoute);
application.use('/api/doctor', doctorsRoute);
application.use('/api/medicament', medicamentsRoute);
application.use('/api/cure', curesRoute);
application.use('/api/department', departmentsRoute);
application.use('/api/position', positionsRoute);

application.listen(serverPort, function () {
    console.log('Started on 0.0.0.0:5000');
});

export default serverPort;
