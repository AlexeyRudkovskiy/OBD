import * as React from "react"
import { render } from 'react-dom'
import { Router, Route, hashHistory, IndexRoute } from "react-router"
import HeaderComponent from "./reports/header"
import DoctorsReport from "./reports/doctors"
import DepartmentsReport from "./reports/departments"
import DepartmentsPatientsReport from "./reports/departments-patients"
import MedicamentsReport from "./reports/medicaments"
import MedicamentDailyReport from "./reports/medicament-destination"
import PatientsReport from "./reports/patients"
import PositionsReport from "./reports/positions"

class ReportsComponent extends React.Component<{}, {}> {
    render() {
        return (
            <div>
                <HeaderComponent />
                <div>
                    <Router history={hashHistory}>
                        <Route path="/doctors" component={DoctorsReport} />
                        <Route path="/departments" component={DepartmentsReport} />
                        <Route path="/departments/patients" component={DepartmentsPatientsReport} />
                        <Route path="/patients" component={PatientsReport} />
                        <Route path="/medicaments" component={MedicamentsReport} />
                        <Route path="/medicaments/daily" component={MedicamentDailyReport} />
                        <Route path="/positions" component={PositionsReport} />
                    </Router>
                </div>
            </div>
        );
    }
}

render(<ReportsComponent />, document.querySelector('#app'));
