import * as React from 'react'
import { render } from 'react-dom'
import { Toggle } from "office-ui-fabric-react/lib/components/Toggle/index";
import { Button } from "office-ui-fabric-react/lib/components/Button/index";
import { Dropdown } from "office-ui-fabric-react/lib/components/Dropdown/index";
import { DayOfWeek, DatePicker } from "office-ui-fabric-react/lib/components/DatePicker/index";
declare function fetch(path:string):Promise<any>;

const DayPickerStrings = {
    months: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ],

    shortMonths: [
        'Янв',
        'Фев',
        'Мар',
        'Апр',
        'Май',
        'Июнь',
        'Июль',
        'Апр',
        'Сен',
        'Окт',
        'Ноя',
        'Дек'
    ],

    days: [
        'Воскресенье',
        'Понедельние',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ],

    shortDays: [
        'Вс',
        'Пн',
        'Вт',
        'Ср',
        'Чч',
        'Пт',
        'Сб'
    ],

    goToToday: 'Выбрать сегодняшнюю дату'
};

interface IDatePickerBasicExampleState {
    firstDayOfWeek?: DayOfWeek;
}

class AppComponent extends React.Component<{}, IDatePickerBasicExampleState>
{

    state: IDatePickerBasicExampleState = {
        firstDayOfWeek: DayOfWeek.Monday
    };

    public onClickListener(): void {
        fetch('/api')
            .then((response:any) => response.json())
            .then(console.log.bind(console));
    }

    render() {
        let { firstDayOfWeek } = this.state;

        return (
            <div>
                <Toggle
                    defaultChecked={ true }
                    label='Enabled and checked'
                    onText='On'
                    offText='Off' />
                <Button onClick={this.onClickListener}>Hello world</Button>

                <DatePicker value={new Date()} formatDate={(date:Date) => date.toLocaleDateString()} firstDayOfWeek={ firstDayOfWeek } strings={ DayPickerStrings } placeholder='Выберите дату' />

                <div className='ms-DropdownBasicExample'>
                    <Dropdown
                        label='Basic example:'
                        options={
                        [
                          { key: 'A', text: 'Option a' },
                          { key: 'B', text: 'Option b' },
                          { key: 'C', text: 'Option c' },
                          { key: 'D', text: 'Option d' },
                          { key: 'E', text: 'Option e' },
                          { key: 'F', text: 'Option f' },
                          { key: 'G', text: 'Option g' },
                          { key: 'H', text: 'Option h', isSelected: true },
                          { key: 'I', text: 'Option i' },
                          { key: 'J', text: 'Option j' },
                        ]
                      }
                                />
                    </div>
            </div>
        );
    }
}

render(<AppComponent />, document.querySelector('#app'));
