import { plural } from 'pluralize'
import {Connection} from "./connection";

export class Model {

    protected table:string;

    protected primary:string = 'id';

    protected dirty:any = {};

    private lockDirty:boolean = true;

    constructor() {
        var className = this.constructor.name.toLowerCase();

        this.table = plural(className);
    }

    /**
     * Find record by primary key
     *
     * @param id
     * @returns {Promise<Model>}
     */
    public find(id:number): Promise<Model> {
        var sql = "select * from `" + this.table + "` where `" + this.primary + "` = " + id;
        var promise = new Promise((accept, reject) => {
            Connection.getInstance().query(sql, [])
                .then(items => {
                    if (items.length < 1) {
                        return reject(null);
                    }
                    (this as any)[this.primary] = items[0][this.primary];
                    return {
                        instance: this,
                        item: items[0]
                    };
                }).then((item:any) => {
                    item = item.item;
                    for (var key in item) {
                        (this as any)[key] = item[key];
                    }
                    return this;
                }).then(item => {
                    this.lockDirty = false;
                    return accept(item);
                }).catch(reject);
        });

        return promise;
    }

    /**
     * Changed fields
     *
     * @returns {any}
     */
    public getDirty(): any {
        return this.dirty;
    }

    protected notifyPropertyChanged(property:string, value:any): void {
        if (!this.lockDirty) {
            this.dirty[property] = value;
        }
    }

    /**
     * Find record by primary key
     *
     * @param args
     * @returns {any}
     */
    public static find(...args:any[]): Promise<Model> {
        var instance = new this();
        return instance.find.apply(instance, arguments);
    }

}
