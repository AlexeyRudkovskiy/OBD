import {createConnection, IConnection, IConnectionConfig, IPoolConfig, IPool} from 'mysql'
import { Promise } from 'es6-promise'
import { default as dbConfig } from '../config/database';
import {DatabaseConnectionException} from "../exception/database-connection-exception";

export class Connection {

    private static _instance:Connection = null;

    private connection:IConnection = null;

    private config:any;

    private checkIsShouldDisconnectInterval:any = null;

    private checkIsShouldDisconnectIntervalInMs = 200;

    private timeout:number = 1000;

    private isDisconnecting:boolean = false;

    private lastQueryDateTime:number = null;

    static getInstance(): Connection {
        if (this._instance == null) {
            this._instance = new Connection;
        }
        return this._instance;
    }

    constructor() {
        this.config = dbConfig;
    }

    public connect(): Connection {
        this.connection = createConnection(this.config as IConnectionConfig);

        this.connection.on('error', this.onConnectionError.bind(this));

        this.connection.connect();

        this.lastQueryDateTime = (new Date()).getTime();
        this.checkIsShouldDisconnectInterval = setInterval(
            this.disconnectIsNecessary.bind(this),
            this.checkIsShouldDisconnectIntervalInMs
        );

        return this;
    }

    public disconnect(): Connection {
        if (this.connection === null) {
            throw new DatabaseConnectionException("You are not connected to the database");
        }
        // this.connection.destroy();
        this.isDisconnecting = true;
        clearInterval(this.checkIsShouldDisconnectInterval);
        return this;
    }

    public query(sql:string, args:any[]): Promise<any> {
        if (this.connection === null) {
            console.log('disconnected');
            this.connect();
        }

        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, function (err, result) {
                if (err) {
                    return reject(err);
                }

                this.lastQueryDateTime = new Date().getTime();

                return resolve(result);
            });
        })
    }

    private disconnectIsNecessary(): void {
        var dateDiff = (new Date()).getTime() - this.lastQueryDateTime;
        if (this.lastQueryDateTime == null || dateDiff > this.timeout && !this.isDisconnecting) {
            // this.disconnect();
        }
    }

    private onConnectionError(e:any): void {
        if (e.code == 'PROTOCOL_CONNECTION_LOST') {
            console.log("error");
            this.connect();
        } else {
            console.log('Error', e);
        }
    }

}
