import * as React from "react"
import { TextField } from "office-ui-fabric-react/lib/components/TextField/index";
import { Button } from "office-ui-fabric-react/lib/components/Button/index";
import { DayOfWeek, DatePicker } from "office-ui-fabric-react/lib/components/DatePicker/index";
import { ChoiceGroup } from "office-ui-fabric-react/lib/components/ChoiceGroup/index";

const DayPickerStrings = {
    months: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
    ],

    shortMonths: [
        'Янв',
        'Фев',
        'Мар',
        'Апр',
        'Май',
        'Июнь',
        'Июль',
        'Апр',
        'Сен',
        'Окт',
        'Ноя',
        'Дек'
    ],

    days: [
        'Воскресенье',
        'Понедельние',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота'
    ],

    shortDays: [
        'Вс',
        'Пн',
        'Вт',
        'Ср',
        'Чч',
        'Пт',
        'Сб'
    ],

    goToToday: 'Выбрать сегодняшнюю дату'
};

interface IDatePickerBasicExampleState {
    firstDayOfWeek?: DayOfWeek;
}

export interface IPatientFormProps {

    name?:string;

    phone?:string;

    birthday?:any;

    gender?:any;

    onSubmit?:any;

}

export class PatientForm extends React.Component<IPatientFormProps, {}> {

    private name:string;

    private phone:string;

    private birthday:any;

    private gender:any;

    constructor() {
        super();

        if (typeof this.props !== "undefined") {
            if (typeof this.props.name !== "undefined")
                this.name = this.props.name;
            if (typeof this.props.phone !== "undefined")
                this.phone = this.props.phone;
            if (typeof this.props.birthday !== "undefined")
                this.birthday = this.props.birthday;
            if (typeof this.props.gender !== "undefined")
                this.gender = this.props.gender;
        }
    }

    render() {
        if (typeof this.name === "undefined") {
            this.name = this.props.name;
        }
        if (typeof this.phone === "undefined") {
            this.phone = this.props.phone;
        }
        if (typeof this.birthday === "undefined") {
            this.birthday = this.props.birthday;
        }
        if (typeof this.gender === "undefined") {
            this.gender = this.props.gender;
        }

        if (typeof this.birthday === "undefined") {
            this.birthday = new Date();
        }

        let firstDayOfWeek = DayOfWeek.Monday;

        return (
            <form className="ms-Grid has-horizontal-offsets">
                <TextField label="ФИО" onChanged={ this.onNameChanged.bind(this) } value={this.props.name} />
                <TextField label="Номер телефона" onChanged={this.onPhoneChanged.bind(this)} value={this.props.phone} />
                <DatePicker label="Дата рождения" value={this.birthday} formatDate={(date:Date) => date.toLocaleDateString()} firstDayOfWeek={ firstDayOfWeek } strings={ DayPickerStrings } placeholder='Выберите дату' onSelectDate={this.onDataSelected.bind(this)} />
                <ChoiceGroup
                    options={[
                        {
                            key: 'м',
                            text: "Мужской"
                        },
                        {
                            key: 'ж',
                            text: "Женский"
                        }
                    ]}
                    required={true}
                    onChanged={this.onGenderChanged.bind(this)} />
                <Button onClick={this.onShowClicked.bind(this)}>Save</Button>
            </form>
        );
    }

    private onNameChanged(value:string) {
        this.name = value;
    }

    private onPhoneChanged(value:any) {
        this.phone = value;
    }

    private onDataSelected(value:any) {
        this.birthday = value;
    }

    private onGenderChanged(value:any) {
        this.gender = value;
    }

    private onShowClicked (e:any) {
        e.preventDefault();
        if (typeof this.props.onSubmit !== "undefined") {
            var data = {
                name: this.name,
                phone: this.phone,
                birthday: this.birthday,
                gender: this.gender.key
            };
            this.props.onSubmit.call(window, data);
        }
    }

}