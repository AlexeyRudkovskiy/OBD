var port:number = 5000;

export function openReport (path:string) {
    const remote = (window as any).require('electron').remote;
    const BrowserWindow = remote.BrowserWindow;

    var url:string = "http://localhost:" + port + "/report.html#" + path;

    console.log(url);

    var win = new BrowserWindow({ width: 800, height: 600 });
    win.loadURL(url);
    win.center();
    win.setMenu(null);
}
