import * as React from "react"
import { Persona, PersonaInitialsColor } from "office-ui-fabric-react/lib/components/Persona/index";
import {Cure} from "../../model/cure";

interface ICureCardStates {
    cure:Cure;

    key:number;
}

export default class CureCard extends React.Component<ICureCardStates, any> {

    constructor() {
        super();
    }

    render() {
        return (
            <div className="cure-card">
                <Persona
                    primaryText={this.props.cure.doctor.fio}
                    secondaryText={this.secondaryText()}
                    imageInitials={this.props.cure.doctor.fio.substr(0, 1)}/>
            </div>
        );
    }

    private secondaryText() {
        return this.props.cure.doctor.position + ' | ' + this.props.cure.doctor.department;
    }

}
