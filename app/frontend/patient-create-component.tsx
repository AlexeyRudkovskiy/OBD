import * as React from "react"
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {Button} from "office-ui-fabric-react/lib/components/Button/index";
import { PatientForm, IPatientFormProps } from "./patient-form"
import {browserHistory} from "react-router";

import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import { IContextualMenuItem } from "office-ui-fabric-react/lib/components/ContextualMenu/ContextualMenu.Props";

export default class PatientCreateComponent extends React.Component<{}, {}> {

    render() {
        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку пациентов",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                />
                <PatientForm onSubmit={this.onSubmit.bind(this)} />
            </div>
        );
    }

    private onSubmit (data:IPatientFormProps) {
        console.log(data.birthday);

        var day:string = ("0" + data.birthday.getDate()).slice(-2);
        var month:string = ("0" + (data.birthday.getMonth() + 1)).slice(-2);

        data.birthday = [day, month, data.birthday.getFullYear()].reverse().join('.');
        // data.birthday = data.birthday.getYear() + '-' + data.birthday.getMonth() + '-' + data.birthday.getDate();
        (window as any).fetch('/api/patient', {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response:any) => {
            browserHistory.push("/");
        });
    }

}