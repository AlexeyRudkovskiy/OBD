import * as React from "react"
import Position from "../../model/position"
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {DetailsList, IColumn, Selection} from "office-ui-fabric-react/lib/components/DetailsList/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {browserHistory} from "react-router";
import {openReport} from "../helpers";

interface IShowComponentStates {

    loaded:boolean;

    positions?:Position[];

    updateSelection?:any;

}

export default class ShowComponent extends React.Component<{}, IShowComponentStates>
{

    private selection:Selection;

    private shouldDelete:number[] = [];

    private positions:any = [];

    state:IShowComponentStates = {
        loaded: false
    };

    constructor() {
        super();

        this.selection = new Selection({
            onSelectionChanged: () => this.setState({
                updateSelection: this.updateSelection(),
                loaded: this.state.loaded
            })
        });
    }

    componentDidMount(): void {
        (window as any).fetch('/api/position')
            .then((response:any) => response.json())
            .then((response:any) => {
                this.positions = response;
                this.setState({
                    positions: response,
                    loaded: true
                })
            });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <Spinner />
            );
        }

        var nameColumn:IColumn = {
            fieldName: "name",
            key: "name",
            minWidth: 150,
            maxWidth: 500,
            name: "Должность"
        };

        var countColumn:IColumn = {
            fieldName: "count",
            key: "count",
            minWidth: 150,
            maxWidth: 200,
            name: "Количество докторов"
        };

        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "Создать должность",
            icon: "Add",
            onClick: function () {
                browserHistory.push('/position/create');
            }
        } as IContextualMenuItem;

        var reportsItems = [
            {
                name: "Список работников",
                key: "workers",
                onClick: () => openReport('/positions')
            }
        ];
        var reports:IContextualMenuItem = {
            key: "reports",
            name: "Отчёты",
            icon: "Document",
            items: reportsItems
        } as IContextualMenuItem;

        var deleteButton:IContextualMenuItem = {
            key: "delete-position",
            name: "Удалить выбранные должности",
            icon: "Delete",
            onClick: function () {
                (window as any).fetch('/api/position/delete', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                            ids: this.shouldDelete
                        }
                    )
                })
                    .then((response:any) => response.json())
                    .then((response:any) => {
                        console.log(response);
                        this.setState({
                            positions: response,
                            loaded: this.state.loaded
                        })
                    });
            }.bind(this)
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        if (this.shouldDelete.length > 0) {
            _items.push(deleteButton);
            reportsItems.push( {
                name: "Список работников выбранных должностей",
                key: "workers-only",
                onClick: () => openReport('/positions?only=' + this.shouldDelete.join(','))
            });
        }

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} farItems={[ reports ]} items={_items} />
                <TextField placeholder={"Введите названия должности для поиска"} onChanged={this.filter.bind(this)} />
                <DetailsList selection={this.selection} items={this.state.positions} columns={[nameColumn, countColumn]} onItemInvoked={this.edit.bind(this)} />
            </div>
        );
    }

    private edit(item:Position) {
        browserHistory.push('/position/edit/' + item.id);
    }

    private updateSelection() {
        this.shouldDelete = this.selection.getSelection().map((p:Position) => p.id);
    }

    private filter(text:string) {
        this.setState({
            loaded: true,
            positions: text ? this.positions.filter((p:any) => p.name.toLowerCase().indexOf(text.toLowerCase()) >= 0) : this.positions
        })
    }

}