import * as React from "react"
import {browserHistory} from "react-router";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";

import {IFormProps, Form} from "./form"

export default class CreateComponent extends React.Component<{}, {}> {

    private name:string;

    render() {

        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "К списку должностей",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/position');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} items={_items} />
                <Form onSave={this.save.bind(this)} />
            </div>
        );
    }

    private save(data:any) {
        (window as any).fetch('/api/position', {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(
            (response:any) => browserHistory.push('/position')
        );
    }

}