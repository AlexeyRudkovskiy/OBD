import * as React from "react"
import {Nav} from "office-ui-fabric-react/lib/components/Nav/index";
import {browserHistory} from "react-router";

var sidebarLayout = [
    {
        links:
            [
                { name: 'Пациенты', url: 'javascript:',    onClick: function (e:any) {browserHistory.push('/');} },
                { name: 'Доктора', url: 'javascript:',     onClick: function (e:any) {browserHistory.push('/doctor');} },
                { name: 'Отделения', url: 'javascript:',   onClick: function (e:any) {browserHistory.push('/department');} },
                { name: 'Должности', url: 'javascript:',   onClick: function (e:any) {browserHistory.push('/position');} },
                { name: 'Медикаменты', url: 'javascript:', onClick: function (e:any) {browserHistory.push('/medicament');}   }
            ]
    }
];

export default class Sdiebar extends React.Component<{}, {}> {

    public render() {
        return (
            <div className="sidebar">
                <Nav groups={sidebarLayout} />
            </div>
        );
    }

}
