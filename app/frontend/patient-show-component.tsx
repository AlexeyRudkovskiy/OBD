import * as React from "react"
import { browserHistory } from "react-router"
import { DetailsList, IColumn, Selection } from "office-ui-fabric-react/lib/components/DetailsList/index";
import { Button } from "office-ui-fabric-react/lib/components/Button/index";
import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import { Spinner } from "office-ui-fabric-react/lib/components/Spinner/index";
import { Pivot, PivotItem } from "office-ui-fabric-react/lib/components/Pivot/index";
import { IContextualMenuItem } from "office-ui-fabric-react/lib/components/ContextualMenu/ContextualMenu.Props";
import {Patient} from "../model/patient";
import {Cure} from "../model/cure";
import CureCard from "./patient/cure-card";

interface IPatientShowComponentStates {

    id?:number;

    patient?:Patient

    selectionDetails?:any;

}

export default class PatientShowComponent extends React.Component<any, IPatientShowComponentStates> {

    state:IPatientShowComponentStates = {
        id: 0
    };

    componentDidMount(): void {
        (window as any).fetch('/api/patient/' + (this.props as any).params.userId)
            .then((response:any) => response.json())
            .then((p:any) => this.setState({
                patient: p as Patient,
                id: (this.props as any).params.userId
            }));
    }

    render() {
        if (typeof this.state.patient === "undefined") {
            return (
                <Spinner />
            );
        }

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку пациентов",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var hdColumn:IColumn = {
            fieldName: "hospitalization_date",
            key: "hospitalization_date",
            minWidth: 150,
            maxWidth: 150,
            name: "Дата госпитализации"
        };

        var dischargedColumn:IColumn = {
            fieldName: "discharged",
            key: "discharged",
            minWidth: 100,
            maxWidth: 100,
            name: "Выписан"
        };

        var doctorColumn:IColumn = {
            fieldName: "doctor",
            key: "doctor",
            minWidth: 100,
            maxWidth: 100,
            name: "Врач"
        };

        var diagnosisColumn:IColumn = {
            fieldName: "diagnosis",
            key: "diagnosis",
            minWidth: 150,
            maxWidth: 250,
            name: "Диагноз"
        };
        var columns:IColumn[] = [hdColumn, dischargedColumn, doctorColumn, diagnosisColumn];

        return (
            <div>
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                />
                <div className="has-offsets">
                    <h2 className="ms-font-xxl patient-name">{ this.state.patient.name }</h2>

                    <Pivot>
                        <PivotItem linkText="О пациенте">
                            <div className="has-offset-top">
                                <div className="ms-Grid grid-info">
                                    <div className="ms-Grid-row">
                                        <div className="ms-Grid-col ms-u-md3 grid-info-item">Дата рождения</div>
                                        <div className="ms-Grid-col ms-u-md9 grid-info-item">{ this.state.patient.birthday }</div>
                                    </div>
                                    <div className="ms-Grid-row">
                                        <div className="ms-Grid-col ms-u-md3 grid-info-item">Пол</div>
                                        <div className="ms-Grid-col ms-u-md9 grid-info-item">{ this.state.patient.gender }</div>
                                    </div>
                                    <div className="ms-Grid-row">
                                        <div className="ms-Grid-col ms-u-md3 grid-info-item">Номер телефона</div>
                                        <div className="ms-Grid-col ms-u-md9 grid-info-item"><a href={['tel:', this.state.patient.phone].join('')}>{ this.state.patient.phone }</a></div>
                                    </div>
                                </div>
                            </div>
                        </PivotItem>
                        <PivotItem linkText="Курсы лечения">
                            <div className="has-offset-top">
                                <Button onClick={this.createCure.bind(this)}>Создать новый курс лечения</Button>
                                <DetailsList onItemInvoked={this.onCureInvoke.bind(this)} items={this.cures()} className={'cures-table'} columns={columns} />
                            </div>
                        </PivotItem>
                    </Pivot>
                </div>
            </div>
        );
    }

    private createCure() {
        browserHistory.push('/patient/createCure/' + this.state.patient.id);
    }

    private onCureInvoke(item:any) {
        browserHistory.push('/cure/show/' + item.id);
    }

    private cures() {
        return this.state.patient.cures.map(function (cure:Cure) {
            return {
                'hospitalization_date': cure.hospitalization_date,
                'discharged': cure.discharge_date == null ? 'нет' : 'да',
                'doctor': cure.doctor.fio,
                'diagnosis': cure.diagnosis,
                'id': cure.id
            };
        });
    }

}
