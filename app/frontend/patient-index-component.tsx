import * as React from "react"
import { browserHistory } from "react-router"
import { DetailsList, IColumn, Selection } from "office-ui-fabric-react/lib/components/DetailsList/index";
import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import { TextField } from "office-ui-fabric-react/lib/components/TextField/index";

import {
    itemsNonFocusable,
    farItemsNonFocusable
} from "office-ui-fabric-react/lib/demo/pages/CommandBarPage/examples/data-nonFocusable";
import { IContextualMenuItem } from "office-ui-fabric-react/lib/components/ContextualMenu/ContextualMenu.Props";
import {Patient} from "../model/patient";
import {openReport} from "./helpers";

interface IWelcomeStates {

    items?:any[];

    selectionDetails?:any;

}

export default class WelcomeComponent extends React.Component<{}, IWelcomeStates> {

    state:IWelcomeStates = {
        items: []
    };

    private items:any[] = [];

    private selection:Selection;

    private shouldDelete:number[] = [];

    constructor() {
        super();

        this.selection = new Selection({
            onSelectionChanged: () => {
                this.setState({
                    selectionDetails: this.updateSelection()
                })
            }
        });
    }

    componentDidMount(): void {

        (window as any).fetch('/api/patient')
            .then((response:any) => response.json())
            .then((response:any) => {
                this.setState({
                    items: response
                });

                this.items = response;
            });
    }

    public onItemClicked (item:any) {
        if (typeof item.id !== "undefined") {
            browserHistory.push('/patient/show/' + item.id);
        }
    }

    render() {
        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "Создать пациента",
            icon: "Add",
            onClick: function () {
                browserHistory.push('/patient/create');
            }
        } as IContextualMenuItem;

        var deleteButton:IContextualMenuItem = {
            key: "delete-patient",
            name: "Удалить выбранных пациентов",
            icon: "Delete",
            onClick: function () {
                (window as any).fetch('/api/patient/delete', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                            ids: this.shouldDelete
                        }
                    )
                })
                    .then((response:any) => response.json())
                    .then((response:any) => this.setState({
                        items: response
                    }));
            }.bind(this)
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var reports:IContextualMenuItem = {
            key: "reports",
            name: "Отчёты",
            icon: "Document",
            items: [
                {
                    name: "Информация о пациентах",
                    key: "statistic",
                    onClick: () => openReport('/patients')
                }
            ]
        } as IContextualMenuItem;

        if (this.shouldDelete.length > 0) {
            _items.push(deleteButton);
        }

        var nameColumn:IColumn = {
            fieldName: "name",
            key: "name",
            minWidth: 150,
            maxWidth: 150,
            name: "ФИО"
        };

        var birthdayColumn:IColumn = {
            fieldName: "birthday",
            key: "birthday",
            minWidth: 150,
            maxWidth: 150,
            name: "Дата рождения"
        };

        var phoneColumn:IColumn = {
            fieldName: "phone",
            key: "phone",
            minWidth: 150,
            maxWidth: 150,
            name: "Номер телефона"
        };

        var genderColumn:IColumn = {
            fieldName: "gender",
            key: "gender",
            minWidth: 150,
            maxWidth: 150,
            name: "Пол"
        };
        var columns:IColumn[] = [nameColumn, birthdayColumn, phoneColumn, genderColumn];

        return (
            <div className="input-borderless">
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                    farItems={[ reports ]}
                />
                <TextField onBeforeChange={this.onSearchChanged.bind(this)} placeholder="Введите фразу для поиска" className="borderless" />
                <DetailsList
                    items={this.state.items}
                    onItemInvoked={this.onItemClicked.bind(this)}
                    columns={columns}
                    selection={this.selection}>
                </DetailsList>
            </div>
        );
    }

    private onSearchChanged(text:string) {
        this.setState({
            items: text ? this.items.filter((item:any) => item.name.toLowerCase().indexOf(text.toLowerCase()) >= 0) : this.items
        });
    }

    private updateSelection() {
        this.shouldDelete = this.selection.getSelection().map((p:Patient) => p.id);
    }

}
