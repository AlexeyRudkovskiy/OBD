import * as React from "react"
import {Dropdown} from "office-ui-fabric-react/lib/components/Dropdown/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {DatePicker, DayOfWeek} from "office-ui-fabric-react/lib/components/DatePicker/index";
import Medicament from "../model/Medicament";
import DatePickerConfig from "./date-picker-config"

export interface ICureMedicamentEditProps {

    medicaments:Medicament[];

    onAnyChanges?:any;

    onDelete?:any;

    data:any;

    key:number;

    id:number;

    hospitalizationDate:Date;

}

export interface ICureMedicamentEditStates {

    medicament_id:number;

    amount:number;

    date:Date;

}

export class CureMedicamentEdit extends React.Component<ICureMedicamentEditProps, any> {

    public key:number = 0;

    private date:Date;

    componentDidMount(): void {
        this.props.data.medicament_id = -1;
        this.props.data.date = this.props.hospitalizationDate;

        this.date = this.props.data.date;
    }

    render() {
        let firstDayOfWeek = DayOfWeek.Monday;

        return (
            <div>
                <DatePicker isMonthPickerVisible={false} label="Дата приёма" value={this.date} formatDate={(date:Date) => date.toLocaleDateString()} firstDayOfWeek={ firstDayOfWeek } strings={ DatePickerConfig } placeholder='Выберите дату' onSelectDate={this.onDateChanged.bind(this)} />
                <Dropdown selectedKey={this.props.data.medicament_id} label="Медикамент" options={this.props.medicaments.map((m:any) => {
                    return { key: m.id, text: m.name }
                })} onChanged={this.onMedicamentChanged.bind(this)} />
                <TextField label="Количество" onChanged={this.onAmountChanged.bind(this)} />
                <div className="delete-button">
                    <a href="javascript:" className="button" onClick={() => this.props.onDelete.call(window, this.props.id)}>удалить</a>
                </div>
                <div className="divider"></div>
            </div>
        );
    }

    private onMedicamentChanged(value:any) {
        this.props.data.medicament_id = value.key;
    }

    private onAmountChanged(value:any) {
        this.props.data.amount = parseFloat(value);
    }

    private onDateChanged(value:any) {
        this.props.data.date = value;
        this.date = value;
    }

}
