import * as React from "react"
import {Patient} from "../model/patient";
import Doctor from "../model/doctor";
import Medicament from "../model/Medicament";
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {Dropdown} from "office-ui-fabric-react/lib/components/Dropdown/index";
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index";
import {Dialog, DialogType, DialogFooter} from "office-ui-fabric-react/lib/components/Dialog/index"
import {DatePicker, DayOfWeek} from "office-ui-fabric-react/lib/components/DatePicker/index"
import CureMedicament from "../model/CureMedicament";
import {CureMedicamentEdit} from "./cure-medicament-edit";
import DatePickerConfig from "./date-picker-config";
import {browserHistory} from "react-router";

import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import { IContextualMenuItem } from "office-ui-fabric-react/lib/components/ContextualMenu/ContextualMenu.Props";

interface IPatientCreateCureStates {
    patient?:Patient;
    doctors?:Doctor[];
    hospitalization_date?:Date;
    medicaments?:Medicament[];
    showDialog?:boolean;
    assignedMedicaments?:any[];
    loading?:boolean;
}

export default class PatientCreateCure extends React.Component<any, IPatientCreateCureStates> {
    private diagnosis:string;
    private doctor_id:number = -1;
    private medicaments:number = 0;
    private hospitalization_date:Date = new Date();
    state:IPatientCreateCureStates = {
        hospitalization_date: new Date(),
        showDialog: false,
        assignedMedicaments: [],
        loading:false
    };

    componentDidMount(): void {
        (window as any).fetch('/api/patient/' + (this.props as any).params.userId)
            .then((r:any) => r.json())
            .then((r:any) => this.setState({ patient: r}));

        (window as any).fetch('/api/doctor/')
            .then((r:any) => r.json())
            .then((r:any) => this.setState({ doctors: r}));

        (window as any).fetch('/api/medicament/')
            .then((r:any) => r.json())
            .then((r:any) => this.setState({ medicaments: r}));
    }

    render() {
        if (this.state.loading || typeof this.state.patient === "undefined" || typeof this.state.doctors === "undefined" || typeof this.state.medicaments === "undefined") {
            return (<Spinner />);
        }
        var patientId = this.state.patient.id;
        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К пациенту",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/patient/show/' + patientId);
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                />
                <form className="ms-Grid has-horizontal-offsets" onSubmit={(e:any) => e.preventDefault()}>
                    <TextField label="Диагноз" onChanged={this.onDiagnosisChanged.bind(this)} />
                    <Dropdown selectedKey={this.doctor_id} label="Врач" onChanged={this.onDoctorChanged.bind(this)}
                              options={ this.state.doctors.map((m:Doctor) => {return { key: m.id, text: m.fio + ", " + m.department + ", пацієнтів: " + (m as any).curses_count }}) } />
                    <DatePicker isMonthPickerVisible={false} label="Дата госпитализации" value={this.hospitalization_date}
                                formatDate={(date:Date) => date.toLocaleDateString()} firstDayOfWeek={ DayOfWeek.Monday }
                                strings={ DatePickerConfig } placeholder='Выберите дату' onSelectDate={this.onDateChanged.bind(this)} />
                    <Button onClick={this.onAddMedicamentsClicked.bind(this)}>Добавить медикаметы и сохранить</Button>

                    <Dialog
                        isOpen={ this.state.showDialog }
                        title='Добавление медикаментов'
                        isBlocking={ true }
                        containerClassName='ms-dialogMainOverride custom-dialog'
                    >

                        <div className="divided-list">
                            {this.state.assignedMedicaments.map((a:any) => <CureMedicamentEdit data={a}
                               key={parseInt(a.key)} id={parseInt(a.key)} medicaments={this.state.medicaments}
                               onAnyChanges={this.onAnyChanges.bind(this)}
                               onDelete={this.onDelete.bind(this)} hospitalizationDate={this.hospitalization_date} />)}
                        </div>

                        <DialogFooter>
                            <Button onClick={this.addMedicament.bind(this)}>добавить медикамент</Button>
                            <Button buttonType={ButtonType.primary} onClick={this.closeDialog.bind(this)}>сохранить</Button>
                        </DialogFooter>
                    </Dialog>
                </form>
            </div>
        );
    }

    private onDiagnosisChanged(val:string) {
        this.diagnosis = val;
    }

    private onDoctorChanged(val:any) {
        this.doctor_id = val.key;
    }

    private addMedicament() {
        this.state.assignedMedicaments.push({
            key: this.medicaments
        });
        this.medicaments++;
        this.setState({
            assignedMedicaments: this.state.assignedMedicaments
        });
    }

    private onAnyChanges (key:string, value:any, target:any) {
        switch (key) {
            case 'medicament_id':
                break;
        }
    }

    private onDateChanged(value:Date) {
        this.hospitalization_date = value;
    }

    private onAddMedicamentsClicked() {
        this.setState({
            showDialog: true
        });
    }

    private onDelete(target:number) {
        var targetIndex:number = -1;
        for (var i = 0; i < this.state.assignedMedicaments.length; i++) {
            if (this.state.assignedMedicaments[i].key === target) {
                targetIndex = i;
                break;
            }
        }

        this.state.assignedMedicaments.splice(targetIndex, 1);
        this.setState({
            assignedMedicaments: this.state.assignedMedicaments
        })
    }

    private closeDialog() {
        this.setState({
            loading: true
        });

        var hd = this.hospitalization_date;


        var day:string =   ("0" + hd.getDate()).slice(-2);
        var month:string = ("0" + (hd.getMonth() + 1)).slice(-2);

        var hdFormated = [day, month, hd.getFullYear()].join('.');

        var createCureArguments = {
            doctor_id: this.doctor_id,
            patient_id: (this.props as any).params.userId,
            hospitalization_date: hdFormated,
            diagnosis: this.diagnosis,
            medicaments: this.state.assignedMedicaments
        };

        (window as any).fetch('/api/patient/createCure', {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(createCureArguments)
        }).then((response:any) => browserHistory.push('/patient/show/' + (this.props as any).params.userId));
    }

}
