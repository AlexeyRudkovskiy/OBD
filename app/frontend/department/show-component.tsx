import * as React from "react"
import Position from "../../model/position"
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {DetailsList, IColumn, Selection} from "office-ui-fabric-react/lib/components/DetailsList/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {browserHistory} from "react-router";
import {Department} from "../../model/department";
import {openReport} from "../helpers";

interface IShowComponentStates {

    loaded:boolean;

    departments?:Position[];

    selectionUpdate?:any;

}

export default class ShowComponent extends React.Component<{}, IShowComponentStates>
{

    private selection:Selection;

    private shouldDelete:number[] = [];

    private departments:any = [];

    state:IShowComponentStates = {
        loaded: false
    };

    constructor() {
        super();

        this.selection = new Selection({
            onSelectionChanged: () => this.setState({
                selectionUpdate: this.updateSelection(),
                loaded: this.state.loaded
            })
        });
    }

    componentDidMount(): void {
        (window as any).fetch('/api/department')
            .then((response:any) => response.json())
            .then((response:any) => {
                this.departments = response;
                this.setState({
                    departments: response,
                    loaded: true
                })
            });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <Spinner />
            );
        }

        var nameColumn:IColumn = {
            fieldName: "name",
            key: "name",
            name: "Отделение"
        } as IColumn;

        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "Создать отделение",
            icon: "Add",
            onClick: function () {
                browserHistory.push('/department/create');
            }
        } as IContextualMenuItem;

        var reportsItems = [
            {
                name: "Информация об отделениях",
                key: "statistic",
                onClick: () => openReport('/departments')
            },
            {
                name: "Пациенты в отделениях",
                key: "patients-in-department",
                onClick: () => openReport('/departments/patients')
            }
        ];
        var reports:IContextualMenuItem = {
            key: "reports",
            name: "Отчёты",
            icon: "Document",
            items: reportsItems
        } as IContextualMenuItem;

        var deleteButton:IContextualMenuItem = {
            key: "delete-department",
            name: "Удалить выбранные отделения",
            icon: "Delete",
            onClick: function () {
                (window as any).fetch('/api/department/delete', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        ids: this.shouldDelete
                    })
                })
                    .then((response:any) => response.json())
                    .then((response:any) => this.setState({
                        departments: response,
                        loaded: true
                    }));
            }.bind(this)
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        if (this.shouldDelete.length > 0) {
            _items.push(deleteButton);
            reportsItems.push({
                name: "Информация о выбранных отделениях",
                key: "report-only",
                onClick: () => openReport('/departments?only=' + this.shouldDelete.join(','))
            });
            reportsItems.push({
                name: "Пациенты в выбранных отделениях",
                key: "report-patients-only",
                onClick: () => openReport('/departments/patients?only=' + this.shouldDelete.join(','))
            });
        }

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} farItems={[reports]} items={_items} />
                <TextField placeholder={"Введите названия отделения для поиска"} onChanged={this.filter.bind(this)} />
                <DetailsList selection={this.selection} items={this.state.departments} onItemInvoked={this.edit.bind(this)} columns={[nameColumn]} />
            </div>
        );
    }

    private updateSelection() {
        this.shouldDelete = this.selection.getSelection().map((d:Department) => d.id);
    }

    private edit(data:any) {
        browserHistory.push('/department/' + data.id);
    }

    private filter(text:string) {
        this.setState({
            loaded: true,
            departments: text ? this.departments.filter((d:Department) => d.name.toLowerCase().indexOf(text.toLowerCase()) >= 0) : this.departments
        })
    }

}