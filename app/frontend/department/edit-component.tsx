import * as React from "react"
import {browserHistory} from "react-router";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {Form} from "./form";
import {Department} from "../../model/department";

interface IEditComponentStates {

    loaded:boolean;

    department?:Department;

}

export default class EditComponent extends React.Component<any, IEditComponentStates> {

    private id:number;

    state:IEditComponentStates = {
        loaded: false
    };

    componentDidMount(): void {
        this.id = (this.props as any).params.id;

        (window as any).fetch('/api/department/' + this.id)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                department: response
            }));
    }

    render () {

        if (!this.state.loaded) {
            return (
                <Spinner />
            );
        }

        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "К списку должностей",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/department');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} items={_items} />
                <Form onSave={this.save.bind(this)} data={this.state.department} />
            </div>
        );
    }

    private save(data:any) {
        (window as any).fetch('/api/department/update/' + this.id, {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(
            (response:any) => browserHistory.push('/department')
        );
    }

}