import * as React from "react"
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index"
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index"

export interface IFormProps { onSave:any; data?:any; }
export interface IFormStates { name:string; }
export class Form extends React.Component<IFormProps, IFormStates> {
    state:IFormStates = { name: "" };
    private name:string = "";
    componentDidMount(): void {
        if (typeof this.props.data !== "undefined") {
            this.setState({ name: (this.props as any).data.name });
            this.name = (this.props as any).data.name;
        }
    }
    render() {
        return (
            <form className="has-horizontal-offsets">
                <TextField value={this.state.name} label="Название" onChanged={this.onChanged.bind(this)} />
                <Button buttonType={ButtonType.primary} onClick={this.save.bind(this)}>Сохранить</Button>
            </form>
        );
    }
    private onChanged(value:string) { this.name = value; }
    private save(e:any) { e.preventDefault(); this.props.onSave.call(this, { name: this.name }); return false; }
}


