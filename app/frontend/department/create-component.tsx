import * as React from "react"
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {browserHistory} from "react-router";
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index"
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index"
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {Form} from "./form"

export default class CreateComponent extends React.Component<{}, {}> {

    private name:string;

    render() {
        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "К списку отделений",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/department');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} items={_items} />
                <Form onSave={this.save.bind(this)} />
            </div>
        );
    }

    private onChanged(value:string) {
        this.name = value;
    }

    private save(data:any) {
        (window as any).fetch('/api/department', {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then(
            (response:any) => browserHistory.push('/department')
        );
    }

}