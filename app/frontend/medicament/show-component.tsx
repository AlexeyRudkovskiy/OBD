import * as React from "react"
import Medicament from "../../model/Medicament";
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {DetailsList, IColumn, Selection} from "office-ui-fabric-react/lib/components/DetailsList/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {browserHistory} from "react-router";
import {openReport} from "../helpers";

interface IShowComponentState {

    loaded:boolean;

    medicaments?:Medicament[];

    selectionUpdate?:any;

}

export default class ShowComponent extends React.Component<{}, IShowComponentState> {

    private medicaments:Medicament[] = [];

    private shouldDelete:number[] = [];

    private selection:Selection;

    state:IShowComponentState = {
        loaded: false
    };

    constructor() {
        super();

        this.selection = new Selection({
            onSelectionChanged: () => this.setState({
                selectionUpdate: this.updateSelection(),
                loaded: this.state.loaded
            })
        });
    }

    componentDidMount(): void {
        (window as any).fetch('/api/medicament')
            .then((response:any) => response.json())
            .then((response:any) => {
                this.medicaments = response;
                this.setState({
                    medicaments: response,
                    loaded: true
                })
            });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <Spinner />
            )
        }

        var nameColumn:IColumn = {
            fieldName: "name",
            key: "name",
            minWidth: 150,
            maxWidth: 600,
            name: "Название медикамента"
        };

        var amountColumn:IColumn = {
            fieldName: "amount",
            key: "amount",
            minWidth: 150,
            maxWidth: 150,
            name: "Остаток на складе"
        };

        var priceColumn:IColumn = {
            fieldName: "price",
            key: "price",
            minWidth: 150,
            maxWidth: 150,
            name: "Цена за еденицу"
        };

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "Создать медикамент",
            icon: "Add",
            onClick: function () {
                browserHistory.push('/medicament/create');
            }
        } as IContextualMenuItem;

        var deleteButton:IContextualMenuItem = {
            key: "delete-medicaments",
            name: "Удалить выбранные медикаменты",
            icon: "Delete",
            onClick: function () {
                (window as any).fetch('/api/medicament/delete', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        ids: this.shouldDelete
                    })
                })
                    .then((response:any) => response.json())
                    .then((response:any) => this.setState({
                        medicaments: response,
                        loaded: true
                    }));
            }.bind(this)
        } as IContextualMenuItem;

        var reportsItems = [
            {
                name: "Статистика",
                key: "statistic",
                onClick: () => openReport('/medicaments')
            }, {
                name: "Назначения по дням",
                key: "daily",
                onClick: () => openReport('/medicaments/daily')
            }
        ];
        var reports:IContextualMenuItem = {
            key: "reports",
            name: "Отчёты",
            icon: "Document",
            items: reportsItems
        } as IContextualMenuItem;

        var _items = [
            item
        ] as IContextualMenuItem[];

        if (this.shouldDelete.length > 0) {
            _items.push(deleteButton);
            reportsItems.push({
                    name: "Только по выделенным",
                    key: "only-selected",
                    onClick: () => openReport('/medicaments?only=' + this.shouldDelete.join(','))
                });
            reportsItems.push({
                name: "Назначения по дням для выбранных",
                key: "daily-only",
                onClick: () => openReport('/medicaments/daily?only=' + this.shouldDelete.join(','))
            });
        }

        return (
            <div>
                <CommandBar items={_items} farItems={[ reports ]} isSearchBoxVisible={false} />
                <TextField placeholder="Поиск медикаментов по их названиям" onBeforeChange={this.filterMedicaments.bind(this)} />
                <DetailsList selection={this.selection} columns={[nameColumn, amountColumn, priceColumn]} items={this.state.medicaments} onItemInvoked={this.edit.bind(this)} />
            </div>
        );
    }

    private filterMedicaments(value:string) {
        this.setState({
            loaded: true,
            medicaments: value ? this.medicaments.filter((m:Medicament) => m.name.toLowerCase().indexOf(value.toLowerCase()) >= 0) : this.medicaments
        });
    }

    private edit(item:any) {
        browserHistory.push('/medicament/' + item.id);
    }

    private updateSelection() {
        this.shouldDelete = this.selection.getSelection().map((m:Medicament) => m.id);
    }

}