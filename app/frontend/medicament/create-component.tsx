import * as React from "react"
import {browserHistory} from "react-router";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {Form, IFormProps} from "./form";

export default class CreateComponent extends React.Component<{}, {}> {

    private name:string;

    private amount:number;

    private price:number;

    render() {
        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку медикаментов",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/medicament');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var initData = {
            name: "",
            price: 0,
            amount: 0
        };

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} items={_items} />
                <Form onSave={this.save.bind(this)} data={initData} />
            </div>
        )
    }

    private save(data:any) {
        (window as any).fetch('/api/medicament', {
            method: 'post',
            headers: {
                'content-type': "application/json"
            },
            body: JSON.stringify(data)
        }).then((response:any) => {
            browserHistory.push('/medicament');
        });
    }

}