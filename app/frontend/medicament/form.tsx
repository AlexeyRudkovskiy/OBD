import * as React from "react"
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index";

export interface IFormProps {

    data:any;

    onSave:any;

}

interface IFormStates {

    name:string;

    amount:number;

    price:number;

}

export class Form extends React.Component<IFormProps, IFormStates> {

    state:IFormStates = {
        name: "",
        amount: 0,
        price: 0
    };

    private name:string;

    private amount:number;

    private price:number;

    componentDidMount(): void {
        this.price = this.props.data.price;
        this.amount = this.props.data.amount;
        this.name = this.props.data.name;

        this.setState({
            name: this.name,
            price: this.price,
            amount: this.amount
        });
    }

    render() {
        return (
            <form className="has-horizontal-offsets">
                <TextField label="Название медикамента" value={this.state.name} onChanged={this.onNameChanged.bind(this)} />
                <TextField label="Количество на складе" value={this.state.amount.toString()} onChanged={this.onAmountChanged.bind(this)} />
                <TextField label="Цена за еденицу" value={this.state.price.toString()} onChanged={this.onPriceChanged.bind(this)} />
                <Button buttonType={ButtonType.primary} onClick={this.save.bind(this)}>Сохранить</Button>
            </form>
        );
    }

    private onNameChanged(value:string) {
        this.name = value;
    }

    private onAmountChanged(value:string) {
        this.amount = parseFloat(value);
    }

    private onPriceChanged(value:string) {
        this.price = parseFloat(value);
        console.log(this.price, value);
    }

    private save(e:any) {
        e.preventDefault();

        this.props.onSave.call(this, {
            name: this.name,
            amount: this.amount,
            price: this.price
        });

        return false;
    }

}