import * as React from "react"
import Medicament from "../../model/Medicament";
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {browserHistory} from "react-router";
import {Form} from "./form";

interface IEditComponent {

    medicament?:Medicament;

    loaded:boolean;

}

export default class EditComponent extends React.Component<any, IEditComponent> {

    private id:number = -1;

    state:IEditComponent = {
        loaded: false
    };

    componentDidMount(): void {
        this.id = (this.props as any).params.id;

        (window as any).fetch('/api/medicament/' + this.id)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                medicament: response
            }));
    }

    render () {
        if (!this.state.loaded) {
            return <Spinner />
        }

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку медикаментов",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/medicament');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var initData = {
            name: "",
            price: 0,
            amount: 0
        };

        return (
            <div>
                <CommandBar items={_items} isSearchBoxVisible={false} />
                <Form onSave={this.save.bind(this)} data={this.state.medicament} />
            </div>
        );
    }

    private save(data:any) {
        (window as any).fetch('/api/medicament/update/' + this.id, {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        }).then((response:any) => browserHistory.push('/medicament'));
    }

}