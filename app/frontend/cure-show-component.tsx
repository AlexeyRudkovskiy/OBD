import * as React from 'react'
import { browserHistory } from "react-router"
import { Spinner } from "office-ui-fabric-react/lib/components/Spinner/index"
import { Cure } from "../model/cure";
import { IContextualMenuItem } from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import { Button } from "office-ui-fabric-react/lib/components/Button/index";
import { DetailsList, IColumn } from "office-ui-fabric-react/lib/components/DetailsList/index";

interface ICureShowComponentStates {

    loaded:boolean;

    cure?:Cure;

}

export default class CureShowComponent extends React.Component<{}, ICureShowComponentStates> {

    private cureId:number = -1;

    state:ICureShowComponentStates = {
        loaded: false
    };

    componentDidMount(): void {
        this.cureId = (this.props as any).params.cureId;
        (window as any).fetch('/api/cure/show/' + this.cureId)
            .then((response:any) => response.json())
            .then((response:any) => {
                this.setState({
                    cure: response,
                    loaded: true
                })
            });
    }

    render() {
        if (!this.state.loaded) {
            return <Spinner />;
        }

        var patientId = this.state.cure.patient_id;

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К пациенту",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/patient/show/' + patientId);
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];


        var dateColumn:IColumn = {
            fieldName: "date",
            key: "date",
            minWidth: 150,
            maxWidth: 150,
            name: "Дата приёма"
        };

        var amountColumn:IColumn = {
            fieldName: "amount",
            key: "amount",
            minWidth: 100,
            maxWidth: 100,
            name: "Количество"
        };

        var nameColumn:IColumn = {
            fieldName: "name",
            key: "name",
            minWidth: 100,
            maxWidth: 100,
            name: "Медикамент"
        };

        var columns:IColumn[] = [dateColumn, amountColumn, nameColumn];

        return (
            <div>
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                />

                <div className="ms-Grid has-offsets has-offset-top color-gray">
                    <div className="ms-Grid-row has-offset-top">
                        <div className="ms-Grid-col ms-u-md4">Дата госпитализации</div>
                        <div className="ms-Grid-col ms-u-md8">{this.state.cure.hospitalization_date}</div>
                    </div>
                    <div className="ms-Grid-row has-offset-top">
                        <div className="ms-Grid-col ms-u-md4">Дата выписки</div>
                        <div className="ms-Grid-col ms-u-md8">{this.state.cure.discharge_date}</div>
                    </div>
                    <div className="ms-Grid-row has-offset-top">
                        <div className="ms-Grid-col ms-u-md4">Врач</div>
                        <div className="ms-Grid-col ms-u-md8">{this.state.cure.doctor}</div>
                    </div>
                    <div className="ms-Grid-row has-offset-top">
                        <div className="ms-Grid-col ms-u-md4">Отделение</div>
                        <div className="ms-Grid-col ms-u-md8">{this.state.cure.department}</div>
                    </div>
                    <div className="ms-Grid-row has-offset-top">
                        <div className="ms-Grid-col ms-u-md12">
                            {this.state.cure.discharge_date == null && <Button onClick={this.discharge.bind(this)}>выписать</Button>}
                            {/*<Button>перевести в другое отделение</Button>*/}
                        </div>
                    </div>
                </div>

                <div className="has-offset-top">
                    {this.state.cure.medicaments.length > 0 && <DetailsList items={this.state.cure.medicaments} columns={columns} />}
                </div>

            </div>
        );
    }

    private discharge() {
        (window as any).fetch('/api/cure/discharge/' + this.cureId)
            .then((response:any) => response.json())
            .then((response:any) => {
                var cure = this.state.cure;
                cure.discharge_date = response.discharge_date;
                this.setState({
                    cure: cure,
                    loaded: true
                });
            });
    }

};
