import * as React from "react"
import {Department} from "../../model/department";
import Position from "../../model/position";
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {Dropdown} from "office-ui-fabric-react/lib/components/Dropdown/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {TextField} from "office-ui-fabric-react/lib/components/TextField/index";
import {Button, ButtonType} from "office-ui-fabric-react/lib/components/Button/index";
import {browserHistory} from "react-router";

interface ICreateComponentInterface {

    loaded:boolean;

    departments?:Department[];

    positions?:Position[];

}

export default class CreateComponent extends React.Component<{}, ICreateComponentInterface> {

    private department_id:number = -1;

    private position_id:number = -1;

    private fio:string;

    state:ICreateComponentInterface = {
        loaded: false,
        departments: null,
        positions: null
    };

    componentDidMount(): void {
        (window as any).fetch('/api/department')
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                departments: response,
                loaded: true
            }));

        (window as any).fetch('/api/position')
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                positions: response,
                loaded: true
            }));
    }

    render() {
        if (!this.state.loaded || this.state.departments == null || this.state.positions == null) {
            return (
                <Spinner />
            );
        }

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку врачей",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/doctor');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        return (
            <div>
                <CommandBar isSearchBoxVisible={false} items={_items} />
                <form className="has-horizontal-offsets">
                    <Dropdown label="Должность" options={this.state.positions.map((a:any) => {return {key:a.id, text: a.name}})} onChanged={this.updatePosition.bind(this)} />
                    <Dropdown label="Отделение" options={this.state.departments.map((a:any) => {return {key:a.id, text: a.name}})} onChanged={this.updateDepartment.bind(this)} />
                    <TextField label="ФИО" onChanged={this.updateFio.bind(this)} />
                    <Button buttonType={ButtonType.primary} onClick={this.createDoctor.bind(this)}>Сохранить</Button>
                </form>
            </div>
        );
    }

    private updatePosition(value:any) {
        this.position_id = value.key;
    }

    private updateDepartment(value:any) {
        this.department_id = value.key;
    }

    private updateFio(value:string) {
        this.fio = value;
    }

    private createDoctor(e:any) {
        e.preventDefault();

        (window as any).fetch('/api/doctor', {
            method: 'post',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                department_id: this.department_id,
                position_id: this.position_id,
                fio: this.fio
            })
        }).then((res:any) => browserHistory.push('/doctor'));

        return false;
    }

}