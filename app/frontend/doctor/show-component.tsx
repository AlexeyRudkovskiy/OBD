import * as React from "react"
import Doctor from "../../model/doctor";
import { Spinner } from "office-ui-fabric-react/lib/components/Spinner/index";
import { DetailsList, IColumn, Selection } from "office-ui-fabric-react/lib/components/DetailsList/index";
import { TextField } from "office-ui-fabric-react/lib/components/TextField/index";
import { CommandBar } from "office-ui-fabric-react/lib/components/CommandBar/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {browserHistory} from "react-router";
import {openReport} from "../helpers";

interface IShowComponentStates {

    doctors?:Doctor[];

    loaded:boolean;

    items?:Doctor[];

    selectionUpdate?:any;

}

export default class ShowComponent extends React.Component<{}, {}> {

    private shouldDelete:number[] = [];

    private selection:Selection;

    state:IShowComponentStates = {
        loaded: false
    };

    constructor() {
        super();

        this.selection = new Selection({
            onSelectionChanged: () => this.setState({
                selectionUpdate: this.updateSelection()
            })
        });
    }

    componentDidMount(): void {
        (window as any).fetch('/api/doctor')
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                doctors: response,
                loaded: true,
                items: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <Spinner />
            );
        }

        var fioColumn:IColumn = {
            fieldName: "fio",
            key: "fio",
            minWidth: 200,
            maxWidth: 200,
            name: "ФИО"
        };

        var positionColumn:IColumn = {
            fieldName: "position",
            key: "position",
            minWidth: 150,
            maxWidth: 150,
            name: "Должность"
        };

        var departmentColumn:IColumn = {
            fieldName: "department",
            key: "department",
            minWidth: 150,
            maxWidth: 150,
            name: "Отделение"
        };

        var item:IContextualMenuItem = {
            key: "add-new-doctor",
            name: "Создать врача",
            icon: "Add",
            onClick: function () {
                browserHistory.push('/doctor/create');
            }
        } as IContextualMenuItem;
        var deleteButton:IContextualMenuItem = {
            key: "delete-doctor",
            name: "Удалить выбранных врачей",
            icon: "Delete",
            onClick: function () {
                (window as any).fetch('/api/doctor/delete', {
                    method: 'post',
                    headers: {
                        'content-type': 'application/json'
                    },
                    body: JSON.stringify({
                        ids: this.shouldDelete
                    })
                })
                    .then((response:any) => response.json())
                    .then((response:any) => this.setState({
                        items: response
                    }));
            }.bind(this)
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var reportsItems = [
            {
                name: "Список работников",
                key: "staff",
                onClick: function () {
                    openReport('/doctors');
                }
            }
        ];
        var reports:IContextualMenuItem = {
            key: "reports",
            name: "Отчёты",
            icon: "Document",
            items: reportsItems
        } as IContextualMenuItem;

        if (this.shouldDelete.length > 0) {
            _items.push(deleteButton);
            reportsItems.push({
                name: "Информация о выбранных работниках",
                key: "staff-only",
                onClick: () => openReport('/doctors?only=' + this.shouldDelete.join(','))
            });
        }

        return (
            <div>
                <CommandBar
                    isSearchBoxVisible={ false }
                    items={ _items }
                    farItems={[ reports ]}
                />
                <TextField placeholder="Поиск по ФИО" onBeforeChange={this.filterItems.bind(this)} />
                <DetailsList selection={this.selection} items={this.state.items} columns={[fioColumn, positionColumn, departmentColumn]} onItemInvoked={this.show.bind(this)} />
            </div>
        );
    }

    private filterItems(value:string) {
        this.setState({
            items: value ? this.state.doctors.filter(d => d.fio.toLowerCase().indexOf(value.toLowerCase()) >= 0) : this.state.doctors
        })
    }

    private show(item:any) {
        browserHistory.push('/doctor/' + item.id);
    }

    private updateSelection() {
        this.shouldDelete = this.selection.getSelection().map((d:Doctor) => d.id);
    }
}