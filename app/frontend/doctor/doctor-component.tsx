import * as React from "react"
import {Spinner} from "office-ui-fabric-react/lib/components/Spinner/index";
import {IContextualMenuItem} from "office-ui-fabric-react/lib/components/ContextualMenu/index";
import {CommandBar} from "office-ui-fabric-react/lib/components/CommandBar/index";
import {DetailsList, IColumn} from "office-ui-fabric-react/lib/components/DetailsList/index";
import Doctor from "../../model/doctor";
import {browserHistory} from "react-router";

interface IDoctorComponentStates {

    loaded:boolean;

    doctor?:Doctor;

}

export default class DoctorComponent extends React.Component<{}, IDoctorComponentStates> {

    private id:number;

    state:IDoctorComponentStates = {
        loaded: false
    };

    componentDidMount(): void {
        this.id = (this.props as any).params.id;
        (window as any).fetch('/api/doctor/' + this.id)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                doctor: response
            }));
    }

    render () {
        if (!this.state.loaded) {
            return <Spinner />
        }

        var item:IContextualMenuItem = {
            key: "add-new-patient",
            name: "К списку докторов",
            icon: "Back",
            onClick: function () {
                browserHistory.push('/doctor');
            }
        } as IContextualMenuItem;
        var _items = [
            item
        ] as IContextualMenuItem[];

        var nameColumn:IColumn = {
            fieldName: "patient",
            key: "patient",
            minWidth: 100,
            maxWidth: 600,
            name: "ФИО"
        };

        var hospitalizationDateColumn:IColumn = {
            fieldName: "hospitalization_date",
            key: "hospitalization_date",
            minWidth: 150,
            maxWidth: 150,
            name: "Дата госпитализации"
        };

        var dischargeDateColumn:IColumn = {
            fieldName: "discharge_date",
            key: "discharge_date",
            minWidth: 100,
            maxWidth: 100,
            name: "Дата выписки"
        };

        return (
            <div>
                <CommandBar items={_items} isSearchBoxVisible={false} />

                <div className="ms-Grid grid-info">
                    <div className="ms-Grid-row">
                        <div className="ms-Grid-col ms-u-md3 grid-info-item">ФИО</div>
                        <div className="ms-Grid-col ms-u-md9 grid-info-item">{ this.state.doctor.fio }</div>
                    </div>
                    <div className="ms-Grid-row">
                        <div className="ms-Grid-col ms-u-md3 grid-info-item">Отделение</div>
                        <div className="ms-Grid-col ms-u-md9 grid-info-item">{ this.state.doctor.department }</div>
                    </div>
                    <div className="ms-Grid-row">
                        <div className="ms-Grid-col ms-u-md3 grid-info-item">Должность</div>
                        <div className="ms-Grid-col ms-u-md9 grid-info-item">{ this.state.doctor.position }</div>
                    </div>
                </div>

                <div>
                    <DetailsList items={(this.state.doctor as any).cures} columns={[nameColumn, hospitalizationDateColumn, dischargeDateColumn]} />
                </div>

            </div>
        );
    }

}