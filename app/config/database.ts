export default {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: '',
  database: 'obd',
  connectionLimit: 10
};