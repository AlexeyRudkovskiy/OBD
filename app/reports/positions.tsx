import * as React from "react"
import Doctor from "../model/doctor";
import {Department} from "../model/department";
import department from "../api/department";

interface IReportState {

    loaded:boolean;

    data?:Department[];

}

export default class PositionsReport extends React.Component<{}, IReportState> {

    state:IReportState = {
        loaded: false
    };

    componentDidMount(): void {
        var url:any = location.hash;
        var only:any = null;
        if (url.indexOf('only=') > -1) {
            url = url.split('only=');
            only = url[1];
        }

        var requestUrl = '/api/position/report';
        if (only != null) {
            requestUrl += '?only=' + only;
        }

        (window as any).fetch(requestUrl)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        var positions:Position[] = this.state.data.map((position:any) => {
            (position as any).key = position.id;
            return position;
        });

        return (
            <table>
                <thead>
                <tr>
                    <th>Отделение</th>
                    <th style={{width: '25%', textAlign: 'right'}}>Кол-во работников</th>
                </tr>
                <tr>
                    <th>ФИО</th>
                    <th style={{textAlign: 'right'}}>Должность</th>
                </tr>
                </thead>
                <tbody>
                {positions.map((position:any) => (
                    [
                        <tr className="group-header" key={position.id}>
                            <td>{position.name}</td>
                            <td style={{textAlign: 'right'}}>{(position as any).doctors.length}</td>
                        </tr>, (position as any).doctors.map((d:Doctor) => (
                        <tr>
                            <td><i>{d.fio}</i></td>
                            <td style={{textAlign: 'right'}}>{d.department}</td>
                        </tr>
                    ))
                    ]
                ))}
                </tbody>
            </table>
        );
    }

}
