import * as React from "react"
import Doctor from "../model/doctor";

interface IReportState {

    loaded:boolean;

    data?:Doctor[];

}

export default class DoctorsReport extends React.Component<{}, IReportState> {

    state:IReportState = {
         loaded: false
    };

    componentDidMount(): void {
        var url:any = location.hash;
        var only:any = null;
        if (url.indexOf('only=') > -1) {
            url = url.split('only=');
            only = url[1];
        }

        var requestUrl = '/api/doctor';
        if (only != null) {
            requestUrl += '?only=' + only;
        }

        (window as any).fetch(requestUrl)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        var doctors:Doctor[] = this.state.data.map((doctor:Doctor) => {
            (doctor as any).key = doctor.id;
            return doctor;
        });

        return (
            <table>
                <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Отделение</th>
                    <th>Должность</th>
                    <th>Курсов лечения</th>
                </tr>
                <tr>
                    <th>ФИО пациента</th>
                    <th>Дата рождения</th>
                    <th>Пол</th>
                    <th>Контактный телефон</th>
                </tr>
                </thead>
                <tbody>
                {doctors.map((doctor:Doctor) => (
                    [<tr key={doctor.id} className="group-header">
                        <td>{doctor.fio}</td>
                        <td>{doctor.department}</td>
                        <td>{doctor.position}</td>
                        <td>{(doctor as any).curses_count}</td>
                    </tr>, (doctor as any).patients.map((p:any) => {
                        return (<tr>
                            <td><i>{p.name}</i></td>
                            <td><i>{p.birthday}</i></td>
                            <td><i>{p.gender}</i></td>
                            <td><i>{p.phone}</i></td>
                        </tr>)
                    })]
                ))}
                </tbody>
            </table>
        );
    }

}
