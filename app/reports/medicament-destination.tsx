import * as React from "react"
import Doctor from "../model/doctor";
import Medicament from "../model/Medicament";

interface IReportState {

    loaded:boolean;

    data?:any[];

}

export default class MedicamentDestinationReport extends React.Component<{}, IReportState> {

    state:IReportState = {
        loaded: false
    };

    componentDidMount(): void {
        var url:any = location.hash;
        var only:any = null;
        if (url.indexOf('only=') > -1) {
            url = url.split('only=');
            only = url[1];
        }

        var requestUrl = '/api/medicament/report/daily';
        if (only != null) {
            requestUrl += '?only=' + only;
        }
        (window as any).fetch(requestUrl)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        return (
            <table>
                <thead>
                <tr>
                    <th colSpan={3}>Наименование</th>
                    <th style={{width: '25%', textAlign: 'right'}}>&nbsp;</th>
                </tr>
                <tr>
                    <th>ФИО</th>
                    <th>Диагноз</th>
                    <th>Дата приёма</th>
                    <th style={{textAlign: 'right'}}>Количество</th>
                </tr>
                </thead>
                <tbody>
                {this.state.data.map((item:any) => (
                    [
                        <tr className="group-header" key={item.date}>
                            <td colSpan={3}>{item.name}</td>
                            <td style={{textAlign: 'right'}}>&nbsp;</td>
                        </tr>, item.daily.map((day:any) => (
                        <tr>
                            <td><i>{day.name}</i></td>
                            <td><i>{day.diagnosis}</i></td>
                            <td><i>{day.date}</i></td>
                            <td style={{textAlign: 'right'}}>{day.amount}</td>
                        </tr>
                    ))
                    ]
                ))}
                </tbody>
            </table>
        );
    }

}
