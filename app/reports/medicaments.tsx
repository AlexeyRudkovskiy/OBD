import * as React from "react"
import Doctor from "../model/doctor";
import Medicament from "../model/Medicament";

interface IReportState {

    loaded:boolean;

    data?:any[];

}

export default class MedicamentsReport extends React.Component<{}, IReportState> {

    state:IReportState = {
        loaded: false
    };

    componentDidMount(): void {
        var url:any = location.hash;
        var only:any = null;
        if (url.indexOf('only=') > -1) {
            url = url.split('only=');
            only = url[1];
        }

        var requestUrl = '/api/medicament/report';
        if (only != null) {
            requestUrl += '?only=' + only;
        }
        (window as any).fetch(requestUrl)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        var data:any[] = [];
        for (var i in this.state.data) {
            if (i != 'null-null') {
                var items:any[] = [];
                for (var j in this.state.data[i]) {
                    items.push(this.state.data[i][j]);
                }
                data.push({
                    date: i,
                    items: items
                });
            }
        }

        return (
            <table>
                <thead>
                <tr>
                    <th>Месяц-Год</th>
                    <th style={{width: '25%', textAlign: 'right'}}>&nbsp;</th>
                </tr>
                <tr>
                    <th>Наименование</th>
                    <th style={{textAlign: 'right'}}>Количество</th>
                </tr>
                </thead>
                <tbody>
                {data.map((item:any) => (
                    [
                        <tr className="group-header" key={item.date}>
                            <td>{item.date}</td>
                            <td style={{textAlign: 'right'}}>&nbsp;</td>
                        </tr>, item.items.map((med:any) => (
                        <tr>
                            <td><i>{med.name}</i></td>
                            <td style={{textAlign: 'right'}}>{med.amount}</td>
                        </tr>
                    ))
                    ]
                ))}
                </tbody>
            </table>
        );
    }

}
