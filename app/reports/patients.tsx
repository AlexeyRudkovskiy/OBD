import * as React from "react"
import Doctor from "../model/doctor";

interface IReportState {

    loaded:boolean;

    data?:Doctor[];

}

export default class PatientsReport extends React.Component<{}, IReportState> {

    state:IReportState = {
        loaded: false
    };

    componentDidMount(): void {
        (window as any).fetch('/api/patient/report')
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        var patients:any[] = this.state.data.map((p:any) => {
            (p as any).key = p.id;
            p.curses = p.curses.map((c:any) => {
                (c as any).key = c.id;
                return c;
            });
            return p;
        });

        return (
            <table>
                <thead>
                <tr>
                    <th>ФИО</th>
                    <th>Дата рождения</th>
                    <th>Пол</th>
                    <th>Номер телефона</th>
                    <th>Курсов лечения</th>
                </tr>
                <tr>
                    <th>Диагноз</th>
                    <th>Лечаший врач</th>
                    <th>Дата госпитализации</th>
                    <th>Дата выписки</th>
                    <th>Отделение</th>
                </tr>
                </thead>
                <tbody>
                {patients.map((p:any) => (
                    [<tr key={p.id} className="group-header">
                        <td>{p.name}</td>
                        <td>{p.birthday}</td>
                        <td>{p.gender}</td>
                        <td>{p.phone}</td>
                        <td>{(p as any).curses.length}</td>
                    </tr>, p.curses.map((c:any) => (
                        <tr>
                            <td><i>{c.diagnosis}</i></td>
                            <td><i>{c.doctor}</i></td>
                            <td><i>{c.hospitalization_date}</i></td>
                            <td><i>{c.discharge_date || 'ещё проходит курс лечения'}</i></td>
                            <td><i>{c.department}</i></td>
                        </tr>
                    ))]
                ))}
                </tbody>
            </table>
        );
    }

}
