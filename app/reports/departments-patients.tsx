import * as React from "react"
import Doctor from "../model/doctor";
import {Department} from "../model/department";
import department from "../api/department";

interface IReportState {

    loaded:boolean;

    data?:Department[];

}

export default class DepartmentsPatientsReport extends React.Component<{}, IReportState> {

    state:IReportState = {
        loaded: false
    };

    componentDidMount(): void {
        var url:any = location.hash;
        var only:any = null;
        if (url.indexOf('only=') > -1) {
            url = url.split('only=');
            only = url[1];
        }

        var requestUrl = '/api/department/patients/report';
        if (only != null) {
            requestUrl += '?only=' + only;
        }

        (window as any).fetch(requestUrl)
            .then((response:any) => response.json())
            .then((response:any) => this.setState({
                loaded: true,
                data: response
            }));
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>
                    Loading...
                </div>
            );
        }

        var departments:Department[] = this.state.data.map((department:Department) => {
            (department as any).key = department.id;
            return department;
        });

        return (
            <table>
                <thead>
                <tr>
                    <th>Отделение</th>
                    <th style={{textAlign: 'right'}}>Всего пациентов</th>
                    <th style={{width: '25%', textAlign: 'right'}}>Кол-во пациентов</th>
                </tr>
                <tr>
                    <th>ФИО</th>
                    <th style={{textAlign: 'right'}}>Лечащий врач</th>
                    <th style={{textAlign: 'right'}}>Дата госпитализации</th>
                </tr>
                </thead>
                <tbody>
                {departments.map((department:Department) => (
                    [
                        <tr className="group-header" key={department.id}>
                            <td>{department.name}</td>
                            <td style={{textAlign: 'right'}}>{(department as any).total}</td>
                            <td style={{textAlign: 'right'}}>{(department as any).patients.length}</td>
                        </tr>, (department as any).patients.map((p:any) => (
                        <tr key={p.id}>
                            <td><i>{p.name}</i></td>
                            <td style={{textAlign: 'right'}}><i>{p.fio}</i></td>
                            <td style={{textAlign: 'right'}}><i>{p.hospitalization_date}</i></td>
                        </tr>
                    ))
                    ]
                ))}
                </tbody>
            </table>
        );
    }

}
