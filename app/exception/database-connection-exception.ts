import ErrnoException = NodeJS.ErrnoException;

export class DatabaseConnectionException {

    public message:string;

    constructor(message: string) {
        this.message = message;
    }

}