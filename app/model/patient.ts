import {Model} from "../orm/model";
import {Cure} from "./cure";

export class Patient extends Model{

    public id:number;

    private _name:string;

    private _birthday:string;

    private _gender:boolean;

    private _phone:string;

    public cures:Cure[] = [];

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;

        this.notifyPropertyChanged('name', value);
    }

    get birthday(): string {
        return this._birthday;
    }

    set birthday(value: string) {
        this._birthday = value;

        this.notifyPropertyChanged('birthday', value);
    }

    get gender(): boolean {
        return this._gender;
    }

    set gender(value: boolean) {
        this._gender = value;

        this.notifyPropertyChanged('gender', value);
    }

    get phone(): string {
        return this._phone;
    }

    set phone(value: string) {
        this._phone = value;

        this.notifyPropertyChanged('phone', value);
    }
}