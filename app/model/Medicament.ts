export default class Medicament {

    public id:number;

    public name:string;

    public amount:number;

    public price:number;

}