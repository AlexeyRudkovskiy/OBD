export default class CureMedicament {

    public cure_id:number;

    public medicament_id:number;

    public date:Date;

    public amount:number;

}
