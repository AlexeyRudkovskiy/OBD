import {Model} from "../orm/model";

export class Department extends Model{

    public id:number;

    private _name:string;

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

}
