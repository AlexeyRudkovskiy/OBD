export class CureMedicamentShow {
    amount:number;
    date:Date;
    medicament_id:number;
    name:string;
}

export class Cure {

    public id:number;

    public doctor:any;

    public department:any;

    public patient_id:number;

    public diagnosis:string;

    public hospitalization_date:Date;

    public discharge_date:Date;

    public medicaments:CureMedicamentShow[] = [];

}