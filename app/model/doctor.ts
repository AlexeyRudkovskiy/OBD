import {Department} from "./department";
export default class Doctor {

    public id:number;

    public fio:string;

    public department:string;

    public position:string;

}