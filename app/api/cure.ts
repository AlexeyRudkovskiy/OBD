import { Router, Response } from "express"
import { formatDate } from "./helpers"

const cure = Router();

cure.get('/show/:id', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');
    var cureId = req.params.id;

    var findCureRequest = "SELECT `cures`.*, `doctors`.`fio` as `doctor`, `departments`.`name` as `department` FROM `cures` INNER join `doctors` on `doctors`.`id` = `doctor_id` inner join `departments` on `departments`.`id` = `doctors`.`department_id` WHERE `cures`.`id` = ?";
    var findMedicaments = "SELECT `cure_medicament`.`amount`, `cure_medicament`.`date`, `cure_medicament`.`medicament_id`, `medicaments`.`name` FROM `cure_medicament` inner join `medicaments` on `medicaments`.`id` = `medicament_id` WHERE `cure_id` = ?";
    var cure = await req.db.query(findCureRequest, [ cureId ]);

    if (cure.length > 0) {
        cure = cure[0];
    } else {
        res.send(JSON.stringify({
            response: "error"
        }));
        return;
    }

    cure.hospitalization_date = formatDate(cure.hospitalization_date);
    cure.medicaments = await req.db.query(findMedicaments, [ cureId ]);

    for (var i = 0; i < cure.medicaments.length; i++) {
        cure.medicaments[i].date = formatDate(cure.medicaments[i].date);
    }

    res.send(JSON.stringify(cure));
});

cure.get('/discharge/:id', async function (req:any, res:Response) {
    var id:number = req.params.id;
    var dischargeRequest = "update `cures` set `discharge_date` = CURRENT_DATE where `id` = ?";
    var findCureRequest = "select `discharge_date` from `cures` where `id` = ?";
    var response:any = await req.db.query(dischargeRequest, [ id ]);
    var cure = await req.db.query(findCureRequest, [ id ]);

    res.send(JSON.stringify({
        discharge_date: formatDate(cure[0].discharge_date)
    }));
});

export default cure;