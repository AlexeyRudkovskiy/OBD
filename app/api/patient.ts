import { Router, Response } from "express"
import { formatDate } from "./helpers"

const patient = Router();

patient.get('/', async function (req:any, res:Response) {
    res.setHeader('Content-type', 'application/json');
    var records = await req.db.query("select * from `patients` order by `id` desc", []);

    for (var i = 0; i < records.length; i++) {
        records[i].birthday = formatDate(records[i].birthday);
    }

    res.send(JSON.stringify(records));
});

patient.get('/report', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var findPatientsRequest = "select * from `patients` order by `name`";
    var findCursesRequest = "SELECT cures.diagnosis, cures.hospitalization_date, cures.discharge_date, departments.name as department, doctors.fio as doctor from cures left join doctors on doctors.id = cures.doctor_id left JOIN departments on departments.id = doctors.department_id where patient_id = ?";

    var patients:any = await req.db.query(findPatientsRequest);
    for (var i = 0; i < patients.length; i++) {
        var p = patients[i];
        p.birthday = formatDate(p.birthday);
        p.curses = await req.db.query(findCursesRequest, [ p.id ]);

        for (var j = 0; j < p.curses.length; j++) {
            p.curses[j].hospitalization_date = formatDate(p.curses[j].hospitalization_date);
            if (p.curses[j].discharge_date != null) {
                p.curses[j].discharge_date = formatDate(p.curses[j].discharge_date);
            }
        }
    }

    res.send(JSON.stringify(patients));
});

patient.get('/:id', async function (req:any, res:Response) {
    res.setHeader('Content-type', 'application/json');
    var findPatientRequest = "select * from `patients` where `id` = ? limit 0, 1";
    var findCuresRequest = "select * from `cures` where `patient_id` = ? order by `id` desc";
    var findDoctorRequest = "SELECT `doctors`.`id`, `doctors`.`fio`, `positions`.`name` as `position`, `departments`.`name` as `department` FROM `doctors` inner join `positions` on `positions`.`id` = `doctors`.`position_id` inner join `departments` on `departments`.`id` = `doctors`.`department_id`  where `doctors`.`id` = ?";
    var findPositionRequest = "select * from `positions` where `id` = ?";
    var findCureMedicamentsRequest = "SELECT `cure`.`amount`, `cure`.`date`, `medicaments`.`name` FROM `cure_medicament` `cure` INNER JOIN `medicaments` on `medicament_id` = `medicaments`.`id` WHERE `cure_id` = ? order by `date` desc";

    var recordRequest = await req.db.query(findPatientRequest, [ req.params.id ]);

    if (typeof recordRequest[0] === "object") {
        var patient = recordRequest[0];
        patient.cures = await req.db.query(findCuresRequest, [ patient.id ]);

        patient.birthday = formatDate(patient.birthday);

        for (var i = 0; i < patient.cures.length; i++) {
            var cure = patient.cures[i];
            cure.hospitalization_date = formatDate(cure.hospitalization_date);
            cure.doctor = await req.db.query(findDoctorRequest, [ cure.doctor_id ]);
            cure.doctor = cure.doctor[0];
            cure.medicaments = await req.db.query(findCureMedicamentsRequest, [ cure.id ]);

            for (var j = 0; j < cure.medicaments.length; j++) {
                cure.medicaments[j].date = formatDate(cure.medicaments[j].date);
            }
        }

        res.send(patient);
    } else {
        res.send(JSON.stringify({
            error: "record not found"
        }));
    }
});

patient.post('/', async function (req:any, res:Response) {
    res.setHeader('Content-type', 'application/json');
    var insertRequest = "insert into `patients` (`name`, `phone`, `birthday`, `gender`) values (?, ?, ?, ?)";

    try {
        var data:any[] = [];
        for (var key in req.body) {
            if (key == 'birthday') {
                var date = formatDate(req.body[key], true);
                data.push(date);
            } else {
                data.push(req.body[key]);
            }
        }
        await req.db.query(insertRequest, data);

        res.send(JSON.stringify({
            response: "success"
        }));
    } catch (e) {
        res.send(JSON.stringify({
            response: "error"
        }));
        console.log(JSON.stringify(e), e);
    }
});

patient.post('/createCure', async function (req:any, res:Response) {
    res.setHeader('Content-type', 'application/json');

    var insertCureRequest = "insert into `cures` (`patient_id`, `doctor_id`, `diagnosis`, `hospitalization_date`) values (?, ?, ?, ?)";
    var insertCureMedicamentRequest = "insert into `cure_medicament` values ";
    var updateMedicamentAmount = "update `medicaments` set `amount` = `amount` - ? where `id` = ?";

    var data:any = req.body;

    data.hospitalization_date = formatDate(data.hospitalization_date, true);

    var createArguments = [
        data.patient_id,
        data.doctor_id,
        data.diagnosis,
        data.hospitalization_date
    ];

    var createdRecord:any = await req.db.query(insertCureRequest, createArguments);

    var cureId = createdRecord.insertId;
    var shouldInsertMedicaments = data.medicaments.length > 0;
    var insertMedicamentsValues:string[] = [];

    var uniqueMedicaments:any = {};

    for (var i = 0; i < data.medicaments.length; i++) {
        var medicament:any = data.medicaments[i];
        var day:any = formatDate(medicament.date, true, '-');
        var insertRow = "(" + cureId + ", " + medicament.medicament_id + ", '" + day + "', " + medicament.amount + ")";
        insertMedicamentsValues.push(
            insertRow
        );

        if (typeof uniqueMedicaments[medicament.medicament_id] === "undefined") {
            uniqueMedicaments[medicament.medicament_id] = medicament.amount;
        } else {
            uniqueMedicaments[medicament.medicament_id] += medicament.amount;
        }
    }

    console.log(insertMedicamentsValues);

    if (shouldInsertMedicaments) {
        console.log(insertCureMedicamentRequest + insertMedicamentsValues.join(','));

        var response:any = await req.db.query(insertCureMedicamentRequest + insertMedicamentsValues.join(','), []);

        for (var k in uniqueMedicaments) {
            await req.db.query(updateMedicamentAmount, [ uniqueMedicaments[k], k ]);
        }
    }

    res.write(JSON.stringify({
        response: "success"
    }));
});

patient.post('/delete', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var deletePatientsRequest = "delete from `patients` where `id` in (";
    deletePatientsRequest += req.body.ids.join(',') + ')';

    await req.db.query(deletePatientsRequest, []);
    var records = await req.db.query("select * from `patients` order by `id` desc", []);

    for (var i = 0; i < records.length; i++) {
        records[i].birthday = formatDate(records[i].birthday);
    }

    res.send(JSON.stringify(records));
});

export default patient;
