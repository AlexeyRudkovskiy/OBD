export function formatDate(date:string, reverse?:any, delimer?:string):string {
    if (typeof reverse === "undefined") {
        reverse = false;
    }
    if (typeof delimer === "undefined") {
        delimer = '-';
    }
    var d = new Date(date);
    if (isNaN(d.getTime())) {
        date = date.split('.').reverse().join('.');
        d = new Date(date);
    }
    var day:string = ("0" + d.getDate()).slice(-2);
    var month:string = ("0" + (d.getMonth() + 1)).slice(-2);

    if (reverse) {
        return [day, month, d.getFullYear()].reverse().join(delimer);
    } else {
        return [day, month, d.getFullYear()].join(delimer);
    }
}