import { Router, Response } from "express"
import {formatDate} from "./helpers";

const medicament = Router();

medicament.get('/report', async function (req:any, res:Response) {

    res.setHeader('content-type', 'application/json');

    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var findMedicamentsRequest = "select * from `medicaments` order by `name`";
    var findHistoryByYearAndMonth = "select medicaments.id, sum(cure_medicament.amount) as sum, YEAR(cure_medicament.date) as year, MONTH(cure_medicament.date) as month from medicaments left join cure_medicament on cure_medicament.medicament_id = medicaments.id left join cures on cures.id = cure_medicament.cure_id where medicaments.id = ? GROUP BY YEAR(cure_medicament.date), MONTH(cure_medicament.date), medicaments.id";

    var medicaments:any = null;
    if (only == null) {
        medicaments = await req.db.query(findMedicamentsRequest);
    } else {
        var sql = "select * from `medicaments` where `id` in (";

        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);

        sql += ") order by `name`";
        medicaments = await req.db.query(sql, []);
    }
    var reportData:any = {};

    for (var i = 0; i < medicaments.length; i++) {
        medicaments[i].history = await req.db.query(findHistoryByYearAndMonth, [ medicaments[i].id ]);

        for (var j = 0; j < medicaments[i].history.length; j++) {
            var history:any = medicaments[i].history[j];
            if (typeof reportData[history.month + '-' + history.year] === "undefined") {
                reportData[history.month + '-' + history.year] = {};
            }
            if (typeof reportData[history.month + '-' + history.year][medicaments[i].id] === "undefined") {
                reportData[history.month + '-' + history.year][medicaments[i].id] = {
                    name: medicaments[i].name,
                    amount: history.sum
                };
            } else {
                reportData[history.month + '-' + history.year][medicaments[i].id].amount += history.sum;
            }
        }
    }

    res.send(JSON.stringify(reportData));
});

medicament.get('/report/daily', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');


    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var findMedicamentsRequest = "select * from `medicaments` order by `name`";
    var findDailyInfo = "select patients.*, cure_medicament.amount, cure_medicament.date, cures.diagnosis, medicaments.id from medicaments inner join cure_medicament on cure_medicament.medicament_id = medicaments.id left join cures on cures.id = cure_medicament.cure_id left join patients on patients.id = cures.patient_id where medicaments.id = ?";

    var medicaments:any = null;
    if (only == null) {
        medicaments = await req.db.query(findMedicamentsRequest);
    } else {
        var sql = "select * from `medicaments` where `id` in (";

        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);

        sql += ") order by `name`";
        medicaments = await req.db.query(sql, []);
    }

    for (var i = 0; i < medicaments.length; i++) {
        medicaments[i].daily = await req.db.query(findDailyInfo, [ medicaments[i].id ]);

        for (var j = 0; j < medicaments[i].daily.length; j++) {
            medicaments[i].daily[j].date = formatDate(medicaments[i].daily[j].date);
        }
    }

    res.send(JSON.stringify(medicaments));

});

medicament.get('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');
    var findMedicamentsRequest = "select * from `medicaments` order by `name`";

    var medicaments = await req.db.query(findMedicamentsRequest, []);

    res.send(JSON.stringify(medicaments));
});

medicament.post('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');
    var createMedicament = "insert into `medicaments` set ?";
    var medicament = await req.db.query(createMedicament, req.body);

    res.send(JSON.stringify(medicament));
});

medicament.get('/:id', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');
    var findMedicamentRequest = "select * from `medicaments` where `id` = ?";
    var medicament = await req.db.query(findMedicamentRequest, [ req.params.id ]);
    if (medicament.length > 0) {
        res.send(JSON.stringify(medicament[0]));
        return;
    }
    res.send(JSON.stringify({
        response: "error"
    }));
});

medicament.post('/update/:id', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');
    var id = req.params.id;
    var updateMedicamentRequest = "update `medicaments` set ? where `id` = " + id;
    var response:any = await req.db.query(updateMedicamentRequest, req.body);

    res.send(JSON.stringify(response));
});

medicament.post('/delete', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var deleteMedicamentsRequest = "delete from `medicaments` where `id` in (" +
        req.body.ids.join(',') + ")";
    var findMedicamentsRequest = "select * from `medicaments` order by `name`";

    await req.db.query(deleteMedicamentsRequest, [ /* empty */ ]);
    var medicaments = await req.db.query(findMedicamentsRequest, []);

    res.send(JSON.stringify(medicaments));
});

export default medicament;
