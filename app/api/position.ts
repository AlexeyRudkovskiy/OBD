import { Router, Response } from "express"
import { formatDate } from "./helpers"

const position = Router();

position.get('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var findDepartmentsRequest = "select *, (select count(`doctors`.`id`) from `doctors` where `position_id` = `positions`.`id`) as count from `positions` order by `name`";
    var departments = await req.db.query(findDepartmentsRequest, []);

    res.send(JSON.stringify(departments));
});

position.post('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var insertPositionRequest = "insert into `positions` set ?";
    var position = await req.db.query(insertPositionRequest, req.body);

    res.send(JSON.stringify(position));
});

position.get('/report', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var findPositionsRequest = "select * from `positions` order by `name`";
    var findDoctorsWithPositionRequest = "select doctors.*, departments.name as department from `doctors` left join departments on departments.id = doctors.department_id where `position_id` = ? order by fio";

    var positions:any = null;
    if (only == null) {
        positions = await req.db.query(findPositionsRequest, []);
    }  else {
        var _only:string = only.join(',');
        var sql = "select * from `positions` where `id` in (";
        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);
        sql += ") order by `name`";
        positions = await req.db.query(sql, []);
    }
    for (var i = 0; i < positions.length; i++) {
        positions[i].doctors = await req.db.query(findDoctorsWithPositionRequest, [ positions[i].id ]);
    }

    res.send(positions);
});

position.get('/:id', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var findPositionRequest = "select * from `positions` where `id` = ?";

    var value = await req.db.query(findPositionRequest, [ req.params.id ]);
    if (value.length > 0) {
        res.send(JSON.stringify(value[0]));
        return;
    }
    res.send(JSON.stringify({
        response: "error"
    }));
});

position.post('/update/:id', async function (req:any, res:Response) {
    var id:number = req.params.id;
    res.setHeader('content-type', 'application/json');

    var updateRequest = "update `positions` set ? where `id` = " + id;
    res.send(JSON.stringify(req.db.query(updateRequest, req.body)));
});

position.post('/delete', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var deletePositionsRequest = "delete from `positions` where `id` in (" +
        req.body.ids.join(',') + ")";
    var findDepartmentsRequest = "select *, (select count(`doctors`.`id`) from `doctors` where `position_id` = `positions`.`id`) as count from `positions` order by `name`";

    await req.db.query(deletePositionsRequest, [ /* empty */ ]);
    var departments = await req.db.query(findDepartmentsRequest, []);

    res.send(JSON.stringify(departments));
});


export default position;