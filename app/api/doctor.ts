import { Router, Response } from "express"
import {formatDate} from "./helpers";

const doctor = Router();

doctor.get('/', async function (req:any, res:Response) {
    var findDoctorsRequest = "SELECT `doctors`.`id`, `doctors`.`fio`, `positions`.`name` as `position`, `departments`.`name` as `department`, count(`cures`.`patient_id`) as `curses_count` FROM `doctors` inner join `positions` on `positions`.`id` = `doctors`.`position_id` inner join `departments` on `departments`.`id` = `doctors`.`department_id` left join `cures` on `cures`.`doctor_id` = `doctors`.`id` GROUP BY `doctors`.`id` order by `doctors`.`fio` asc";
    var findPatientsRequest = "SELECT patients.* from cures left join patients on patients.id = cures.patient_id where cures.doctor_id = ? group by patients.id";

    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var doctors:any = null;
    if (only == null) {
        doctors = await req.db.query(findDoctorsRequest, []);
    } else {
        var sql = "SELECT `doctors`.`id`, `doctors`.`fio`, `positions`.`name` as `position`, `departments`.`name` as `department`, count(`cures`.`patient_id`) as `curses_count` FROM `doctors` inner join `positions` on `positions`.`id` = `doctors`.`position_id` inner join `departments` on `departments`.`id` = `doctors`.`department_id` left join `cures` on `cures`.`doctor_id` = `doctors`.`id` where doctors.id in (";

        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);

        sql += ") GROUP BY `doctors`.`id` order by `doctors`.`fio` asc";
        doctors = await req.db.query(sql, []);
    }

    for (var i = 0; i < doctors.length; i++) {
        doctors[i].patients = await req.db.query(findPatientsRequest, [ doctors[i].id ]);
        for (var j = 0; j < doctors[i].patients.length; j++) {
            doctors[i].patients[j].birthday = formatDate(doctors[i].patients[j].birthday);
        }
    }

    res.send(JSON.stringify(doctors));
});

doctor.post('/', async function (req:any, res:Response) {
    var createDoctor = "insert into `doctors` set ?";
    var d = await req.db.query(createDoctor, req.body);

    res.send(JSON.stringify(d));
});

doctor.get('/:id', async function (req:any, res:Response) {

    var id:number = req.params.id;

    var findDoctorRequest = "select `doctors`.*, `departments`.`name` as `department`, `positions`.`name` as `position`, count(`cures`.`id`) as `cures_count` from `doctors` inner join `departments` on `departments`.`id` = `doctors`.`department_id` inner join `positions` on `positions`.`id` = `doctors`.`position_id` left join `cures` on `cures`.`doctor_id` = `doctors`.`id` where `doctors`.`id` = ? group by `doctors`.`id`;";
    var findCuresRequest = "select cures.*, patients.name as patient from cures inner join patients on patients.id = cures.patient_id where doctor_id = ?";

    var doctor = await req.db.query(findDoctorRequest, [ id ]);
    if (doctor.length > 0) {
        doctor = doctor[0];
        doctor.cures = await req.db.query(findCuresRequest, [ id ]);

        for (var i = 0; i < doctor.cures.length; i++) {
            doctor.cures[i].hospitalization_date = formatDate(doctor.cures[i].hospitalization_date);
            doctor.cures[i].discharge_date = formatDate(doctor.cures[i].discharge_date);
        }

        res.send(doctor);
        return;
    }
    res.send(JSON.stringify({
        response: "error"
    }));
});

doctor.post('/delete', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var deleteDoctorsRequest = "delete from `doctors` where `id` in (";
    deleteDoctorsRequest += req.body.ids.join(',') + ')';
    var findDoctorsRequest = "SELECT `doctors`.`id`, `doctors`.`fio`, `positions`.`name` as `position`, `departments`.`name` as `department` FROM `doctors` inner join `positions` on `positions`.`id` = `doctors`.`position_id` inner join `departments` on `departments`.`id` = `doctors`.`department_id` order by `doctors`.`fio` asc ";

    await req.db.query(deleteDoctorsRequest, [ /* empty */ ]);
    var doctors = await req.db.query(findDoctorsRequest, []);

    res.send(JSON.stringify(doctors));
});

export default doctor;
