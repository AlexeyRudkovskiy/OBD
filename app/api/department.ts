import { Router, Response } from "express"
import { formatDate } from "./helpers"

const department = Router();

department.get('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var findDepartmentsRequest = "select * from `departments` order by `name`";
    var departments = await req.db.query(findDepartmentsRequest, []);

    res.send(JSON.stringify(departments));
});

department.get('/report', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var findDepartmentsRequest = "select * from `departments` order by `name`";
    var findDoctorRequest = "select `doctors`.`fio`, `positions`.`name` as `position` from `doctors` left join `positions` on `positions`.`id` = `doctors`.`position_id` where `department_id` = ? order by `fio`";

    var departments:any = null;
    if (only == null) {
        departments = await req.db.query(findDepartmentsRequest, []);
    } else {
        var _only:string = only.join(',');
        var sql = "select * from `departments` where `id` in (";
        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);
        sql += ") order by `name`";
        departments = await req.db.query(sql, []);
    }

    for (var i = 0; i < departments.length; i++) {
        var department = departments[i];
        department.doctors = await req.db.query(findDoctorRequest, [ department.id ]);
    }

    res.send(JSON.stringify(departments));
});

department.get('/patients/report', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var only:any = null;
    if (typeof req.query.only !== "undefined") {
        only = req.query.only.split(',');
    }

    var findDepartmentsRequest = "select * from `departments` order by `name`";
    var findPatients = "select patients.*, doctors.fio, cures.hospitalization_date from departments inner join doctors on doctors.department_id = departments.id inner join cures on cures.doctor_id = doctors.id inner join patients on patients.id = cures.patient_id where cures.discharge_date is null and departments.id = ?";
    var findAllPatientsCount = "select count(cures.id) as count from cures left join doctors on doctors.id = cures.doctor_id where department_id = ?";

    var departments:any = null;
    if (only == null) {
        departments = await req.db.query(findDepartmentsRequest, []);
    }  else {
        var _only:string = only.join(',');
        var sql = "select * from `departments` where `id` in (";
        for (var i = 0; i < only.length; i++) {
            sql += only[i].toString() + ','
        }
        sql = sql.substr(0, sql.length - 1);
        sql += ") order by `name`";
        departments = await req.db.query(sql, []);
    }

    for (var i = 0; i < departments.length; i++) {
        var department = departments[i];
        department.patients = await req.db.query(findPatients, [ department.id ]);
        department.total = (await req.db.query(findAllPatientsCount, [department.id]))[0].count;

        for (var j = 0; j < department.patients.length; j++) {
            department.patients[j].hospitalization_date = formatDate(department.patients[j].hospitalization_date);
        }
    }

    res.send(JSON.stringify(departments));
});

department.post('/', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var insertDepartmentRequest = "insert into `departments` set ?";
    var department = await req.db.query(insertDepartmentRequest, req.body);

    res.send(JSON.stringify(department));
});

department.post('/delete', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var deleteDepartmentsRequest = "delete from `departments` where `id` in (" +
        req.body.ids.join(',') + ")";
    var findDepartmentsRequest = "select * from `departments` order by `name`";

    await req.db.query(deleteDepartmentsRequest, [ /* empty */ ]);
    var departments = await req.db.query(findDepartmentsRequest, []);

    res.send(JSON.stringify(departments));
});

department.get('/:id', async function (req:any, res:Response) {
    res.setHeader('content-type', 'application/json');

    var findPositionRequest = "select * from `departments` where `id` = ?";

    var value = await req.db.query(findPositionRequest, [ req.params.id ]);
    if (value.length > 0) {
        res.send(JSON.stringify(value[0]));
        return;
    }
    res.send(JSON.stringify({
        response: "error"
    }));
});

department.post('/update/:id', async function (req:any, res:Response) {
    var id:number = req.params.id;
    res.setHeader('content-type', 'application/json');

    var updateRequest = "update `departments` set ? where `id` = " + id;
    res.send(JSON.stringify(req.db.query(updateRequest, req.body)));
});

export default department;
