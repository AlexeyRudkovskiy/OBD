import { plural } from 'pluralize'
import { Router, Response } from "express"

const routeProxy = Router();

export default class CRUDProxy {

    private table:string;

    public route:Router = Router();

    public constructor (private prefix:string) {
        this.table = plural(prefix);

        this.route.get('/', this.index);
        this.route.get('/:id', this.show);
    }

    private async index(req:any, res:Response) {
        var records = await req.db.query("select * from `patients`");
        res.send(JSON.stringify(records));
    }

    private async show(req:any, res:Response) {
        var record = await req.db.query("select * from `patients` where `id` = " + req.params.id);
        res.send(JSON.stringify(record[0]));

    }

}