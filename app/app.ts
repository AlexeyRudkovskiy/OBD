import BrowserWindowOptions = Electron.BrowserWindowOptions;

import serverPort from "./apiserver"

const electron = require('electron');
const {app} = electron;
const {BrowserWindow} = electron;
const installer = require('electron-devtools-installer');

var window:Electron.BrowserWindow  = null;

function createWindow() {
    window = new BrowserWindow({
        width: 800,
        height: 640
    } as BrowserWindowOptions);

    window.center();
    window.setMenu(null);

    installer.default('REACT_DEVELOPER_TOOLS');


    // todo: replace with scalable code
    window.loadURL("http://localhost:" + serverPort)
    window.webContents.openDevTools({
        mode: "undocked"
    });

    window.on("closed", () => { window = null; });
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

app.on("activate", () => {
    if (window === null) {
        createWindow();
    }
});
