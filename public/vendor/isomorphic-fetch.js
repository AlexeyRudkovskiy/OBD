;(function() {
    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
        module.exports = window.fetch;

        // RequireJS
    } else if (typeof define === "function" && define.amd) {
        define([], function () {
            return window.fetch;
        });

        // <script>
    } else {
        var g;
        if (typeof window !== "undefined") {
            g = window;
        } else if (typeof global !== "undefined") {
            g = global;
        } else if (typeof self !== "undefined") {
            g = self;
        } else {
            g = this;
        }
        g.fetch = window.fetch;
    }

})(function(fetch) {
    return fetch.fetch;
});