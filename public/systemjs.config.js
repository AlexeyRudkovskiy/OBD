System.config({
    defaultJSExtensions: true,
    map: {
        'react': '/node_modules/react/dist/react.js',
        'react-dom': '/node_modules/react-dom/dist/react-dom.js',
        'react-router': '/node_modules/react-router/umd/ReactRouter',
        'isomorphic-fetch': '/vendor/isomorphic-fetch.js',
        'office-ui-fabric-react': '/node_modules/office-ui-fabric-react/',
        'office-ui-fabric-react.min': '/vendor/office-ui-fabric-react.min.js',
        '@microsoft/load-themed-styles': '/node_modules/@microsoft/load-themed-styles/lib/index.js'
    }
});

System.import("/dist/frontend.js").catch(console.error.bind(console));