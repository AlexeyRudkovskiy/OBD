import * as React from "react";
import {Sidebar} from "./sidebar/Sidebar";
import {Article} from "./article/Article";

export interface IApplicationProps {

}

export interface IApplicationStates {
    clicks:number;
    text?:string;
}

export class Application extends React.Component<IApplicationProps, IApplicationStates> {

    state: IApplicationStates = {
        clicks: 10,
        text: "Hello world"
    };

    constructor() {
        super();
        this.increment = this.increment.bind(this);
    }

    increment() {
        this.setState({clicks: this.state.clicks + 1})
    }

    render() {
        return (
            <div>
                <Sidebar/>
                <button onClick={this.increment}>{this.state.clicks}</button>
                <Article/>
            </div>
        );
    }
}