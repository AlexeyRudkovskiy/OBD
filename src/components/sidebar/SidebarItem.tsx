import * as React from 'react'

export class SidebarItemProps {

    constructor(text: string, href: string, key:number = 0) {
        this.text = text;
        this.href = href;
        this.key = key;
    }

    key?:number;

    text:string;
    href:string;
}

export class SidebarItem extends React.Component<SidebarItemProps, {}> {

    render() {
        return (
            <a href={this.props.href}>{this.props.text}</a>
        );
    }

}