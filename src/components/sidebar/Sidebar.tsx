import * as React from 'react'
import {SidebarItem, SidebarItemProps} from "./SidebarItem";

export interface ISidebarProps {}

export interface ISidebarStates {
    items:SidebarItemProps[];
}

export class Sidebar extends React.Component<ISidebarProps, ISidebarStates>
{

    state: ISidebarStates = {
        items: [
            new SidebarItemProps("VK", "http://vk.com/", 1),
            new SidebarItemProps("FB", "http://fb.com/", 2)
        ]
    };

    render() {
        var sidebarItems:any = [];
        this.state.items.forEach(function (item:SidebarItemProps) {
            sidebarItems.push(<SidebarItem href={item.href} text={item.text} key={item.key} />);
        });


        return (
            <div className="sidebar">
                {sidebarItems}
            </div>
        );
    }

}
