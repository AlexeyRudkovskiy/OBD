import * as React from 'react'
import * as fetch from 'isomorphic-fetch';
import { IComment, Comment } from "./Comment";

export interface IArticleProps {}

export interface IArticleStates {

    title:string;
    description:string;
    loaded:boolean;
    comments:IComment[];

}

export class Article extends React.Component<IArticleProps, IArticleStates>
{

    state:IArticleStates = {
        title: "article title",
        description: "article description",
        loaded:false,
        comments: [
            {
                id: 1,
                comment: "Hello world"
            }
        ]
    };

    constructor() {
        super();
        fetch('/article.json')
            .then((response:any) => response.json())
            .then(this.setState.bind(this))
            .then(() => {
                this.setState({
                    loaded: true
                } as IArticleStates);
            });
    }

    render() {
        if (!this.state.loaded) {
            return (
                <div>Loading...</div>
            );
        }

        var comments = this.state.comments.map(comment => <Comment key={comment.id} comment={comment} />);

        return (
            <div>
                <h1>{ this.state.title }</h1>
                <p>{ this.state.description }</p>

                <div>
                    {comments}
                </div>

                <button onClick={this.onclick.bind(this)}>Insert comment</button>

            </div>
        );
    }

    private onclick() {
        this.setState({
            comments: this.state.comments.concat([{
                id: Math.random() * 10000,
                comment: "New comment"
            } as IComment])
        } as IArticleStates);
    }

}
