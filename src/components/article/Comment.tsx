import * as React from 'react'

export class IComment {

    id:number;
    comment:string;

}

export interface ICommentProps {
    comment:IComment;
}

export interface ICommentStates {

}

export class Comment extends React.Component<ICommentProps, ICommentStates>{

    render() {
        return (
            <div data-id={this.props.comment.id}>
                {this.props.comment.comment}
            </div>
        );
    }

}